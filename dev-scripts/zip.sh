#!/bin/bash 
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CDIR=$( pwd )
cd $DIR/../themes
zip -r $DIR/../zips/jeo-theme.zip jeo-theme -x "jeo-theme/node_modules/*"