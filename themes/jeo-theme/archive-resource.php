<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Newspack
 */

get_header();
?>
		<section id="primary" class="content-area custom-archive">

			<?php do_action( 'before_archive_posts' ); ?>

			<main id="main" class="site-main">
                <header class="page-header">
                    <span>
                        <h1 class="page-title article-section-title category-header">
                            <?= _x( 'Resources for Land and Rights Defense', 'Title archive', 'jeo' ) ?>
                         </h1>
                        <div class="type-description">
                            <?php dynamic_sidebar('resource_archive_description') ?>
                        </div>
                    </span>
                </header><!-- .page-header -->

                <div class="terms">
                    <?php 
                        $terms = get_terms('resource_type', [
                            'orderby' => 'title',
                            'orderby' => 'meta_value_num',
                            'order' => 'ASC',
                            'hide_empty' => false,
                            'meta_query' => [[
                                'key' => 'order',
                                'type' => 'NUMERIC',
                            ]],
                        ]);
                    
                        set_query_var( 'block_params', ['taxonomy' => 'resource_type', 'terms' => $terms, 'style' => 'is-style-with_description']);
                        get_template_part( 'template-parts/content/taxonomy', 'terms');
                    ?>
                </div>

                <?php 
                    $filters = [
                        // [   
                        //     "type" => "taxonomy",
                        //     "placeholder" => __('Resource type', 'jeo'), 
                        //     "filterSettings" => [ 
                        //         "taxonomy" => 'resource_type', 
                        //         "multipleSelection" => true, 
                        //     ] 
                        // ],
                        [
                            "type"           => "taxonomy",
                            "placeholder"    => __('Subject', 'jeo'),
                            "filterSettings" => [
                                "taxonomy"          => 'post_tag',
                                "multipleSelection" => true,
                            ]
                        ],
                        [   
                            "type" => "taxonomy",
                            "placeholder" => __('Author', 'jeo'), 
                            "filterSettings" => [ 
                                "taxonomy" => 'aauthor', 
                                "multipleSelection" => true, 
                            ] 
                        ],
                        [   
                            "type" => "taxonomy",
                            "placeholder" => __('Format', 'jeo'), 
                            "filterSettings" => [ 
                                "taxonomy" => 'nformat', 
                                "multipleSelection" => true, 
                            ] 
                        ]
                    ];

                    $card_model = "ResourceCard";
                    $baseURL = get_rest_url(null, '/wp/v2/resource');

                    $terms = get_terms('resource_type', [
                        'orderby' => 'title',
                        'order' => 'desc',
                        'hide_empty' => true,
                    ]);

                    $terms_meta = [];

                    foreach($terms as $term) {
                        $icon_meta = get_term_meta($term->term_id, 'icon', true);
                        $color_meta = get_term_meta($term->term_id, 'color', true);

                        $terms_meta[$term->term_id] = [
                            'color' => $color_meta,
                            'icon' => $icon_meta,
                        ];
                    }

                    $data_building = [
                        'filters' => $filters, 
                        'cardModel' => $card_model,
                        'baseURL' => $baseURL,
                        'addicionalCardMeta' => ['terms_meta' => $terms_meta],
                    ];
                
                ?>

		        <div id="faceted-posts-search" data-building="<?= htmlentities(json_encode($data_building)) ?>"></div>
                
			</main><!-- #main -->
			<aside class="category-page-sidebar">
    			<div class="content">
					<?php dynamic_sidebar('resources_sidebar') ?>
				</div>
			</aside>
		</section><!-- #primary -->
<?php
get_footer();
