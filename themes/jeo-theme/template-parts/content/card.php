<?php
include get_stylesheet_directory() . '/inc/post-types-taxonomies.php'; 

$show_image = isset($show_image) ? $show_image : true;
$show_excerpt = isset($show_excerpt) ? $show_excerpt : true;
$show_taxonomy = isset($show_taxonomy) ? $show_taxonomy : true;
$show_author = isset($show_author) ? $show_author : true;
$show_date = isset($show_date) ? $show_date : true;
$replace_taxonomy_with_post_type = isset($replace_taxonomy_with_post_type) ? $replace_taxonomy_with_post_type : false;

?>



<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php if($show_image): ?>
        <?php newspack_post_thumbnail(); ?>
    <?php endif; ?>

	<div class="entry-container">
		<?php
        if ( $show_taxonomy ) :
            if(!$replace_taxonomy_with_post_type) {
                newspack_categories();
            } else {
                $section_name = @$post_type_labels[get_post_type()];
				
				echo '<span class="cat-links">';

                if(isset($section_name)) {
                    echo $section_name;
                } else {
					echo get_post_type();
				}

				echo '</span>';
            }
		endif;
		?>
		<header class="entry-header">
			<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
		</header><!-- .entry-header -->

		<?php if($show_date): ?>
			<div class="entry-meta">
				<?php newspack_posted_by(); ?>
				<?php newspack_posted_on(); ?>
			</div><!-- .meta-info -->
		<?php endif; ?>
		
		<?php if($show_excerpt): ?>
			<div class="entry-content">
				<?php the_excerpt(); ?>
			</div><!-- .entry-content -->
		<?php endif; ?>
	</div><!-- .entry-container -->
</article><!-- #post-${ID} -->

