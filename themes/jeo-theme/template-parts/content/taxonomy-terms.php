<?php
$block_params = get_query_var('block_params');
$terms = $block_params['terms'];
$style = $block_params['style'];
$taxonomy = $block_params['taxonomy'];
$show_description = isset($block_params['show_description'])? $block_params['show_description'] : false;
?>


<div class="taxonomy-terms <?php echo $style; ?>">
    <?php if(!sizeof($terms)): ?>
        <h4>
            <?= __("No terms found to show! Sorry ;(", "jeo")?>
        </h4>
    <?php endif; ?>
        
    <?php if ($style == "is-style-with_description") : ?>
        <?php foreach ($terms as $term) : ?>
            <?php 
                $icon_meta    = get_term_meta($term->term_id, 'icon', true);
                $color_meta   = get_term_meta($term->term_id, 'color', true);
                $text_see_all = get_term_meta($term->term_id, 'text_see_all', true);
            ?>

            <div class="taxonomy-terms-card with-description" style="--block-color: <?php echo $color_meta? $color_meta : 'var(--primary)' ?>">
                <div class="card-header">
                    <?php if ($icon_meta): ?>
                        <i class="<?= $icon_meta ?>"></i>
                    <?php endif; ?>
                    
                    <div class="sideway">
                        <h5 class="title">
                            <a href="<?= get_term_link($term) ?>">
                                <?= $term->name ?>
                            </a>
                        </h5>
                        <?php if(strlen($term->description)): ?>
                            <!-- <i class="fas fa-minus-circle"></i> -->
                        <?php endif; ?>
                    </div>
                </div>
                <?php if(strlen($term->description)): ?>
                    <div class="card-content">
                        <p>
                            <?php echo $term->description ?>
                        </p>
                            <?php if ($text_see_all): ?>
                                <a href="<?= get_term_link($term) ?>"><?= esc_html($text_see_all) ?> <?= $term->name ?></a>
                            <?php else: ?>
                                <a href="<?= get_term_link($term) ?>"><?= __("See all ", "jeo") ?> <?= $term->name ?></a>
                            <?php endif; ?>
                    </div>
                <?php endif; ?>
                
            </div>
        <?php endforeach ?>
    <?php else : ?>
        <?php foreach ($terms as $term) : ?>
            <?php

                /**
                 * Filters the meta_key used for the terms featured image.
                 * 
                 * Default `background_image`
                 */
                $featured_image_meta = apply_filters( 'block_taxonomy_terms_featured_image_meta_key', 'background_image', $taxonomy );

                $background_meta = get_term_meta( $term->term_id, $featured_image_meta, true );
                $color_meta      = get_term_meta( $term->term_id, 'color', true ) ? get_term_meta( $term->term_id, 'color', true ) : 'var(--primary)';
                $text_see_all    = get_term_meta( $term->term_id, 'text_see_all', true );
                $image_url       = false;

                if ( $background_meta && isset( $background_meta['ID'] ) ) {
                    $image_url = wp_get_attachment_image_src( $background_meta['ID'], 'full' )[0];
                }

            ?>

            <?php if ( $style == "is-style-with_description_external_title" ) : ?>

                <div class="taxonomy-terms-card">
                    <a href="<?= get_term_link($term) ?>" class="taxonomy-terms-card-link" >
                        <div class="thumb" style="background-image: url(<?= $image_url ?>);">
                        </div>
                        <h3 class="title">
                            <?php echo $term->name ?>
                        </h3>
                    </a>
                </div><!-- /.taxonomy-terms-card -->

            <?php else : ?>

                <?php if(!$show_description): ?>
                    <a href="<?= get_term_link($term) ?>" class="taxonomy-terms-card" style="background-image: url(<?= $image_url ?>); --block-color: <?= $color_meta ?>">
                <?php else: ?>
                    <a class="taxonomy-link" href="<?= get_term_link($term) ?>">
                        <div class="taxonomy-terms-card <?= strlen($term->description)? 'with-expanded-description' : '' ?>" style="--background-image: url(<?= $image_url ?>); --block-color: <?= $color_meta ?>">
                <?php endif; ?>
                            <?= $show_description? '<div class="featured-image-bg">' : '' ?>
                                <h3 class="title">
                                    <?php echo $term->name ?>
                                </h3>
                                <div class="quantity">
                                    <i class="fas fa-th-large"></i>
                                    <span> <?php echo $term->count . " " . __("cards", "jeo") ?> </span>
                                </div>
                            <?= $show_description? '</div>' : '' ?>

                            <?php if($show_description): ?>
                                <div class="description">
                                    <p><?php echo $term->description ?></p>
                                    <?php if ($text_see_all): ?>
                                        <p class="see-all"><?= esc_html($text_see_all) ?> <?= $term->name ?></p>
                                        <?php else: ?>
                                            <p class="see-all"><?= __("See all ", "jeo") ?> <?= $term->name ?></p>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>  
                <?php if(!$show_description): ?>
                    </a>
                <?php else: ?>
                        </div>
                    </a>
                <?php endif; ?>

            <?php endif; ?>

        <?php endforeach ?>

        <?php if( ! $show_description && $style != "is-style-with_description_external_title" ): ?>
            <a class="taxonomy-terms-card all-terms d-md-none" href="<?= esc_url(get_post_type_archive_link("knowledge_exchange")) ?>"><span><?= __("KNOW IT ALL ", "jeo") ?> <i class="fas fa-arrow-right"></i></span></a>
        <?php endif; ?>
    <?php endif; ?>

</div>
