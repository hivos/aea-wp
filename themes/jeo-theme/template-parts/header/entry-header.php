<?php
/**
 * Displays the post header
 *
 * @package Newspack
 */
include get_stylesheet_directory() . '/inc/post-types-taxonomies.php'; 

$discussion = !is_page() && newspack_can_show_post_thumbnail() ? newspack_get_discussion_data() : null;


if (is_singular()) : ?>
    <?php
    if (!is_page()) :
        $post_type = get_post_type();
        if(in_array($post_type, $supported_types)) {
            $mathing_info = $matching_criterias[$post_type];
            get_equivalence_taxs($post_type, $mathing_info['label'], $mathing_info['taxonomy']);
        } else {
            newspack_categories();
        }
    endif;
    ?>
    <?php
    $subtitle = get_post_meta($post->ID, 'newspack_post_subtitle', true);
    ?>

    <div class="wrapper-entry-title">
        <h1 class="entry-title <?php echo $subtitle ? 'entry-title--with-subtitle' : ''; ?>">
            <?php echo wp_kses_post(get_the_title()); ?>
        </h1>
    </div>
    <?php if ($subtitle) : ?>
        <div class="newspack-post-subtitle">
            <?php echo esc_html($subtitle); ?>
        </div>
    <?php endif; ?>

<?php else : ?>
    <h2 class="entry-title">
        <a href="<?php the_permalink(); ?>" rel="bookmark">
            <?php echo wp_kses_post(get_the_title()); ?>
        </a>
    </h2>
<?php endif; ?>


<?php if(has_excerpt() && !('large' == newspack_featured_image_position() || 'small' == newspack_featured_image_position())): ?>
    <h1 class="post-excerpt">
        <?php
            global $post;
            
            if(! boolval(get_theme_mod('disable_excerpt_in_all_posts', false)) && ! boolval(get_post_meta($post->ID, 'hide_post_excerpt', true ))) {
                the_excerpt();
            }
        ?>
    </h1>
<?php endif ?>

<?php if (!is_page() && 'behind' !== newspack_featured_image_position() && !get_query_var('hide_post_meta')) : ?>
    <div class="entry-subhead">
        <div class="entry-meta">
            <div class="author-partner">
                <?php newspack_posted_by(); ?>
            </div>
            <?php newspack_posted_on(); ?>
        </div><!-- .meta-info -->
        
        
        <?php
        // Display Jetpack Share icons, if enabled
        if (function_exists('sharing_display')) {
            sharing_display('', true);
        }
        ?>
    </div>

    

<?php endif; ?>
