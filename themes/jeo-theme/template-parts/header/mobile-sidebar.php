<?php

/**
 * Template to display the mobile navigation, either AMP or fallback.
 *
 * @package Newspack
 */
include get_stylesheet_directory() . '/inc/post-types-taxonomies.php'; 

if (newspack_is_amp()) : ?>
	<amp-sidebar id="mobile-sidebar" layout="nodisplay" side="right" class="mobile-sidebar">
		<button class="mobile-menu-toggle" on='tap:mobile-sidebar.toggle'>
			<?php echo wp_kses(newspack_get_icon_svg('close', 20), newspack_sanitize_svgs()); ?>
			<?php esc_html_e('Close', 'newspack'); ?>
		</button>
	<?php else : ?>
		<aside id="mobile-sidebar-fallback" class="mobile-sidebar">
			<button class="mobile-menu-toggle">
				<?php echo wp_kses(newspack_get_icon_svg('close', 20), newspack_sanitize_svgs()); ?>
			</button>
		<?php endif; ?>

		<?php

		newspack_primary_menu();

		$button_url = get_theme_mod('discovery_button_link');


		if (!empty($button_url)) : ?>
			<div class="discovery-menu">
				<div class="discovery-title">
					<a href="<?= $button_url ?>" class="discovery-link">
						<?= __('Discovery', 'jeo') ?>
					</a>
				</div>
			</div>
		<?php endif;

		newspack_tertiary_menu();

		?>

		<div class="more-menu">
			<div class="more-title">
				<?php
				$more_name = esc_html(wp_get_nav_menu_name('more-menu'));
				if (strlen($more_name) <= 0) {
					$more_name = __('MORE', 'jeo');
				}
				?>
				<span class="more-name"><?= $more_name ?></span>
			</div>

			<div class="more-menu--content">
				<?php
				$nav = wp_nav_menu(
					array(
						'theme_location' => 'more-menu',
						'container'      => false,
						'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'          => 1,
						'fallback_cb'    => false,
						'echo'           => false,
					)
				);
				?>
				<?php if ($nav) : ?>
					<div class="item">
						<div class="item--title language-title">
							<?= __("Language", "jeo") ?>
						</div>
						<div class="item--content language-item-content">
							<?php echo $nav; ?>
						</div>
					</div>
				<?php endif; ?>

				<div class="item">
					<div class="item--title">
						<?= __("Type size", "jeo") ?>
					</div>

					<div class="item--content padded">
						<button action="increase-size"><i class="fas fa-font"></i>+</button>
						<button action="decrease-size"><i class="fas fa-font"></i>-</button>
					</div>
				</div>

				<!-- <div class="item">
					<div class="item--title">
					</div>

					<div class="item--content padded">
						<button action="increase-contrast">
							<i class="fas fa-adjust"></i>+
						</button>
						<button action="decrease-contrast">
							<i class="fas fa-adjust"></i>-
						</button>
					</div>
				</div>-->


			</div>
		</div>
		
		<div class="social-menus">
			<div class="social-menus--title">
				<?= __("Follow us", "jeo") ?>
			</div>
			<?php
			newspack_social_menu_header();
			?>
		</div>

		

		<?php if (newspack_is_amp()) : ?>
	</amp-sidebar>
<?php else : ?>
	</aside>
<?php endif; ?>

<div class="mobile-toolbar">
	<div class="wrapper">

		<?php 
		foreach($supported_types_toolbar as $post_type_label) {
			$post_type_term = $matching_criterias_toolbar[$post_type_label];
			?>
			<div class="item <?= $post_type_label ?>" >
				<button action="toggle-options">
					<a href="<?= esc_url(get_post_type_archive_link($post_type_label)) ?>">
						<?= $post_type_term['icon'] ?>

						<div class="item--title">
							<?= $post_type_term['label_toolbar'] ?>
						</div>
					</a>
				</button>
			</div>
		<?php } ?>

		<div class="item case-studies">
            <button action="toggle-options">
                <a href="<?= esc_url(get_page_url_by_template_name('template-case-studies.php')) ?>">
                
                    <i class="fas fa-map"></i>

                    <div class="item--title">
                        <?= __('Case studies', 'jeo') ?>
                    </div>
                </a>
            </button>
		</div>

		<div class="item share">
			<button action="share-navigator">
				<i class="fas fa-share-alt"></i>

				<div class="item--title">
					<?= __("Share", "jeo") ?>
				</div>
			</button>
		</div>
	</div>

</div>