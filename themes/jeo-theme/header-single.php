<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Newspack
 */
?><!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php

	do_action('wp_body_open');
	do_action('before_header');

	// Header Settings
	$header_simplified     = get_theme_mod('header_simplified', false);
	$header_center_logo    = get_theme_mod('header_center_logo', false);
	$show_slideout_sidebar = get_theme_mod('header_show_slideout', false);
	$header_sub_simplified = get_theme_mod('header_sub_simplified', false);

	get_template_part('template-parts/header/mobile', 'sidebar');
	get_template_part('template-parts/header/desktop', 'sidebar');

	if (true === $header_sub_simplified && !is_front_page()) :
		get_template_part('template-parts/header/subpage', 'sidebar');
	endif;
	?>

	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#content"><?php _e('Skip to content', 'newspack'); ?></a>
		
		<header class="header">
		<div class="header--sticky">
				<div class="wrapper">
					<div class="left-content">
						<button class="hamburger-menu" aria-label="<?= __("Toggle menu visibility", 'jeo') ?>">
							<i class="fas fa-bars"></i>
						</button>

						<a href="<?= home_url() ?>" class="logo">
							<?php
								if(has_custom_logo()) {
									$custom_logo_id = get_theme_mod( 'custom_logo' );
									$image = wp_get_attachment_image( $custom_logo_id , 'full' );
									echo $image;
								}
							?>
						</a>
					</div>

					<div class="center-content">
						<span class="post-title">
							<?php echo wp_kses_post(get_the_title()); ?>
						</span>
					</div>

					<div class="right-content">
						<div class="language-area">
							<?php if(has_action('wpml_language_switcher')):?>
								<div class="language-switter">
									<span><?= __("Choose a language:", "jeo") ?></span>
									<?= do_action( 'wpml_language_switcher', [
										'native' => 0,
										'link_current' => 0,
									],  ); ?>
								</div>
							<?php endif; ?>

							<button action="toggle-language-switcher">
								<?= constant('ICL_LANGUAGE_CODE') ?>
							</button>
						</div>
						
						<div class="social-icons">
							<?php get_template_part( 'template-parts/content/content', 'social-networks' ); ?>
						</div>
                        
                        <button class="search" aria-label="<?= __("Toggle search area visibility", 'jeo') ?>">
							<i class="fas fa-search"></i>
						</button>
					</div>

				</div>
			</div>	

			<div class="header--subheader">
				<div class="wrapper">
					<div class="social-menu">
						<?= __("Follow us: ", "jeo") ?>
						<?php newspack_social_menu_header() ?>
					</div>

					<div class="languages-menu">
						<?php if(has_action('wpml_language_switcher')):?>
							<div class="language-switter">
								<span><?= __("Choose a language:", "jeo") ?></span>
								<?= do_action( 'wpml_language_switcher', [
									'native' => 0
								] ); ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>

			<div class="header--main">
				<div class="wrapper">
					<button class="hamburger-menu" aria-label="<?= __("Toggle menu visibility", 'jeo') ?>">
						<i class="fas fa-bars"></i>
                        <span> <?= __("Menu") ?></span>
					</button>

					<?php
						$header_center_logo  = get_theme_mod( 'header_center_logo', false );
					?>

					<div class="site-header">

						<?php
							if(!empty(get_theme_mod( 'logo_dark_image', '' ))) { ?>
							<a href="<?= home_url() ?>">
							<?php
								echo wp_get_attachment_image(
									get_theme_mod( 'logo_dark_image', '' ),
									'newspack-footer-logo',
									'',
									array( 'class' => 'header-logo dark-logo' )
								); ?>
							</a>
							<?php }
							child_newspack_the_custom_logo();					
							
						?>
					</div><!-- .site-branding -->


					<button class="search" aria-label="<?= __("Toggle search area visibility", 'jeo') ?>">
						<i class="fas fa-search"></i>
					</button>
				</div>
			</div>

			<div class="header--content">
				<?php
					wp_nav_menu(
						array(
							'theme_location' => 'primary-menu',
							'menu_class'     => 'main-menu',
							'container'      => false,
							'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						)
					);
				?>

				<ul class="more-menu main-menu">
					<li class="more-title">
						<a href="">
							<?php
							$more_name = esc_html(wp_get_nav_menu_name('more-menu'));
							if (strlen($more_name) <= 0) {
								$more_name = __('MORE', 'jeo');
							}
							?>
							<span class="more-name"><?= $more_name ?></span>
							<button class="submenu-expand" tabindex="-1"><svg class="svg-icon" width="24" height="24" aria-hidden="true" role="img" focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M7.41 8.59L12 13.17l4.59-4.58L18 10l-6 6-6-6 1.41-1.41z"></path><path fill="none" d="M0 0h24v24H0V0z"></path></svg></button>
						</a>

						<ul class="more-menu--content sub-menu">
							<li class="item">
                                <div class="item--title language-title">
                                    <?= __("Language", "jeo") ?>
                                </div>
                                <div class="item--content language-item-content">
                                    <?= do_action( 'wpml_language_switcher', [
										'native' => 0,
										// 'link_current' => 0,
									],  ); ?>
                                </div>
                            </li>

							<li class="item">
								<div class="item--title">
									<?= __("Font size", "jeo") ?>
								</div>

								<div class="item--content padded">
									<button action="increase-size"><i class="fas fa-font"></i>+</button>
									<button action="decrease-size"><i class="fas fa-font"></i>-</button>
								</div>
							</li>

							<!-- <div class="item">
								<div class="item--title">
								</div>

								<div class="item--content padded">
									<button action="increase-contrast">
										<i class="fas fa-adjust"></i>+
									</button>
									<button action="decrease-contrast">
										<i class="fas fa-adjust"></i>-
									</button>
								</div>
							</div>-->
						</ul>
					</li>

					
				</ul>

				<div class="follow-us">
					<span><?= __("Follow us", 'jeo') ?></span>
					<?php newspack_social_menu_header() ?>
				</div>
			</div>

			<div class="header--search">
				<div class="content-limiter">
					<h3><?= __("What are you looking for?", "jeo") ?></h3>
					<form action="<?php echo esc_url(home_url('/')); ?>">
						<input type="hidden" name="lang" value="<?php echo(ICL_LANGUAGE_CODE) ?>">
						<input type="search" name="s" id="s" placeholder="<?= __("Type here your search...", "jeo") ?>">
						<button action="submit">
							<svg class="svg-icon" width="28" height="28" aria-hidden="true" role="img" xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path><path d="M0 0h24v24H0z" fill="none"></path></svg>
						</button>
					</form>
				</div>

				
			</div>
		</header>

		<?php
		if (function_exists('yoast_breadcrumb')) {
			yoast_breadcrumb('<div class="site-breadcrumb desktop-only"><div class="wrapper">', '</div></div>');
		}
		?>

		<?php do_action('after_header'); ?>

		<div id="content" class="site-content decoration-<?= get_theme_mod('decoration_style', 'square') ?>">