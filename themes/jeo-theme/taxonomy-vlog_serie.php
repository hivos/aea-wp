<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Newspack
 */

get_header();

$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
$icon_meta = get_term_meta($term->term_id, 'icon', true);
$color_meta = get_term_meta($term->term_id, 'color', true);
$background_meta = get_term_meta($term->term_id, 'background', true);
$image_url = false;

if($background_meta) {
    $image_url = wp_get_attachment_image_src($background_meta['ID'], 'full')[0];
}
?>
        <header class="page-header" style="--term-image: url('<?= $image_url ?>')">
            <div class="wrapper">
                <h3 class="taxonomy-title">
                    <a href="<?php echo esc_url( home_url( 'videos' ) ); ?>">
                        <?=  __( 'Videos about the amazon', 'jeo' ) ?>
                    </a>
                </h3>
                <div class="term-title">
                    <h1>
                        <?php single_term_title(); ?>
                    </h1>
                </div>

                <?php if ( '' !== get_the_archive_description() ) : ?>
                    <div class="term-description">
                        <?php echo wp_kses_post( wpautop( get_the_archive_description() ) ); ?>
                    </div>
                <?php endif; ?>

            </div>
        </header><!-- .page-header -->

        <header class="mobile-only">
            <div class="wrapper">
                <?php if ( '' !== get_the_archive_description() ) : ?>
                    <div class="term-description">
                        <?php echo wp_kses_post( wpautop( get_the_archive_description() ) ); ?>
                    </div>
                <?php endif; ?>

            </div>
        </header>

		<section id="primary" class="content-area custom-archive">
			<?php do_action( 'before_archive_posts' ); ?>

			<main id="main" class="site-main">
                <?php
                    $filters = [
                        [
                            "type"           => "taxonomy",
                            "placeholder"    => __('Subject', 'jeo'),
                            "filterSettings" => [
                                "taxonomy"          => 'post_tag',
                                "multipleSelection" => true,
                            ]
                        ],
                        [   
                            "type" => "taxonomy",
                            "placeholder" => __('Author', 'jeo'), 
                            "filterSettings" => [ 
                                "taxonomy" => 'aauthor', 
                                "multipleSelection" => true, 
                            ] 
                        ],
                        [
                            "type" => "date-range-picker",
                            "placeholder" => __('Date', 'jeo'),
                        ],

                    ];

                    $card_model = "VideoCard";

                    $baseURL = get_rest_url(null, 'wp/v2/video?vlog_serie=' . $term->term_id);

                    $data_building = [
                        'filters' => $filters,
                        'cardModel' => $card_model,
                        'baseURL' => $baseURL,
                        'addicionalCardMeta' => [],
                        // 'addicionalCardMeta' => [
                        //     'taxonomies_listed' => ['post_tag'], // any taxonomy supported by post type
                        //     'format' => 'vertical', // horizontal or vertical. Default is horizontal
                        // ],
                    ];

                ?>

		        <div id="faceted-posts-search" data-building="<?= htmlentities(json_encode($data_building)) ?>"></div>

			</main><!-- #main -->

			<aside class="category-page-sidebar">
    			<div class="content">
					<?php dynamic_sidebar('vlog_sidebar') ?>
				</div>
			</aside>
		</section><!-- #primary -->
<?php
get_footer();