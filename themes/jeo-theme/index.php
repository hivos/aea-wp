<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Newspack
 */

get_header();

?>

	<section id="primary" class="content-area custom-archive">
			<header class="page-header">
				<span>
					<?php single_post_title( '<h4 class="page-title article-section-title">', '</h4>' ); ?>
				</span>

			</header><!-- .page-header -->
		<main id="main" class="site-main">
		<?php
			$filters = [
				[   
					"type"           => "taxonomy",
					"placeholder"    => __('Type', 'jeo'),
					"filterSettings" => [
						"taxonomy"          => 'category',
						"multipleSelection" => true,
					] 
				],
				[
					"type"           => "taxonomy",
					"placeholder"    => __('Subject', 'jeo'),
					"filterSettings" => [
						"taxonomy"          => 'post_tag',
						"multipleSelection" => true,
					]
				],
				[   
					"type" => "taxonomy",
					"placeholder" => __('Author', 'jeo'), 
					"filterSettings" => [ 
						"taxonomy" => 'aauthor', 
						"multipleSelection" => true, 
					] 
				],
				[   
					"type" => "date-range-picker",
					"placeholder" => __('Date', 'jeo'), 
				],

			];

			$card_model = "PostCard";
			$baseURL = get_rest_url(null, 'wp/v2/posts');

			$data_building = [
				'filters' => $filters, 
				'cardModel' => $card_model,
				'baseURL' => $baseURL,
                'useInfiniteLoader' => false,
				'addicionalCardMeta' => [  
                    'taxonomies_listed' => ['category'],
                    'format' => 'horizontal',
                ],
			];

		?>
		<div id="faceted-posts-search" data-building="<?= htmlentities(json_encode($data_building)) ?>"></div>

		</main><!-- .site-main -->

		<!-- old: get_sidebar() -->
        <aside class="category-page-sidebar">
    		<div class="content">
				<?php dynamic_sidebar('category_page_sidebar') ?>
			</div>
		</aside>
	</section><!-- .content-area -->

<?php
get_footer();
