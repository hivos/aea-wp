��    	     d      �      �     �     �     �     �     �     �     �     
          #     7     ?     L     \     l     z     �     �     �     �     �     �  	   �     �  
     
             $     *     0     7     >     P     d     t     �     �  	   �     �     �     �  X   �     $     +     A     N  
   [     f  +   v     �     �  )   �     �     �     �          +     <     N     ^     c     r     z  U   �     �     �     �     �     	     !     )  	   6     @  	   W     a     g     v  F   �     �     �     �               +     4     F  
   V  	   a     k     w  	   �  
   �     �  #   �     �     �     �     �     �     	                                   #     5     T  
   h  _   s     �     �     �  :         ;     I  
   Y     d  	   z     �     �     �     �  	   �     �     �     �     �               *     1     A     I     d     t      �     �     �     �  	   �     �     �       
        (     -     2     7     =  .   U     �  :   �     �  4   �               "     .     3     <     K     ]      r  	   �     �     �     �     �     �     �  
   �     �  %   �     "     9     P     U     j     {     �     �     �     �     �     �               '     5     K  	   Y     c     l  	   �  %   �     �     �     �     �     �     �     �     �     
  	        $     7     =  :   E     �     �     �     �     �  	   �     �  	   �     �     �     �     �     �  +   �       3        Q     X     [     b     e     m     r  
   �     �     �     �     �     �  	   �  
   �     �     �                 )   6   2   A   i      �      �      �      �      �      �      �      �      �      �      �      	!     !     !     !  �  (!     #     #  1   #     =#     ?#  ,   B#     o#     }#     �#     �#     �#     �#     �#     �#     $     $     2$     P$     o$     �$     �$     �$     �$     �$     �$     �$     �$     �$     �$     %     %     %     1%     G%     [%     p%     �%     �%     �%     �%     �%  X   �%     !&     *&     <&     L&     \&     k&  2   }&     �&     �&  1   �&     �&     	'  $   '  #   6'     Z'     q'     �'     �'     �'     �'     �'  i   �'     K(     P(     ](     f(  "   �(     �(     �(  
   �(     �(     �(     �(     �(     )  S   #)     w)     �)     �)     �)  &   �)  	   �)     �)     *     *     '*     :*  *   N*     y*     �*     �*  +   �*     �*     �*     �*     �*     +     +     +     +     +     !+     $+     '+     /+      F+     g+  
   +  |   �+     ,     ,     *,  8   ;,     t,     �,     �,     �,     �,     �,     �,     �,  
   �,     �,     �,     -  '   8-  /   `-     �-     �-     �-     �-     �-     �-     �-     �-  *   .  "   F.     i.     o.     �.     �.     �.     �.  
   �.     �.     �.     /     /     /  5   +/     a/  K   f/     �/  D   �/     0     0     80     H0  	   M0     W0     n0     �0  6   �0     �0     �0     1     1     !1     )1     51  
   A1     L1  ,   ]1     �1     �1     �1     �1     �1  #   �1     !2     *2     22     A2     I2     R2     a2     v2     �2     �2     �2  
   �2     �2     �2     �2  .   3     13     F3     J3     Q3     ]3  	   q3  	   {3     �3     �3     �3     �3     �3  	   �3  ?   �3     (4     14     >4     B4  
   J4     U4     a4  
   u4     �4     �4     �4     �4     �4  2   �4     �4  0   �4     5     5      5     )5     -5     55     :5     S5     a5     y5     �5     �5     �5     �5  	   �5     �5     �5     �5     �5     6  K   6  S   d6     �6     �6     �6     �6     �6     �6     �6     7     7     7     7  
   7     7  	   &7     07    -   and  %s comments found. See comments , ,  1 comment found. See comment ABOUT THE AUTHOR About the writer Academy Access project page Add New Add New Item Add custom css: Add new article Add new flyer Add new section Add republish link Add republish link:  Additional Content: Additional Information Advanced Search All All Items All episodes All series All videos Apply April Audio August Author Authors biography Authors biography:  Authors listing Authors listing:  Back to the home Bullet widget Bullets:  Button URL: Button text: By By disabling this, the header will use the default background with some opacity applied. Cancel Cards with background Case Studies Case studies Case study Category weight Check to enable the "sorry we said wrong":  Choose a language: Clear Click in this button to load more results Close Search Contact Contact form shortcode: Contact template sidebar Container style: Content Box Title Content options Copy Copyright logo Country Custom Dark logo to be displayed. The default dark mode logo is your logo masked with white. Date Date weight December Decoration marker color Decoration marker style Default Description: Discovery Discovery button style Edit Item Error Example: Dosis Example: Libre Basquesville Example: Open Sans Condensed. If it is empty, the Header Font is used. Facebook Featured Image Featured Slider Featured episodes Featured image setting: February Filter items list Filter the list Flyer list Follow us Follow us:  Font Awesome Icon Class: Font size Font sizes Font sizes mobile Force font-size by using !important Format Fr From Get Involved Get involved area H1 H2 H3 H4 H5 H6 Hacklab Hide post excerpt Hide post excerpt in all posts Hide post excerpt:  Horizontal If the generic option to hide posts in Customizer Panel is enabled, it will replace this option Image Gallery Initiative by Insert into item It looks like there are no posts matching this criteria ;( Item Archives Item Attributes Items list Items list navigation JEO Theme January July June KNOW IT ALL  Knowledge Knowledge Exchange Knowledge exchange Knowledge exchange sidebar Knowledge exchange type sidebar Language Last edition link: Latest Latest episodes Latests Learning CommunitySubject Leave a comment Leave empty to use default Leave empty to use primary color Leave it empty to hide. Line Link Dropdown Load more Login to academy link Logo dark image Logo sticky image MM/DD/YYYY MORE Mail Maps March Marker background image Maximum quantity of posts shown in the widget: May Media partner link (It works with selected media partners) Menu font size (rem) Mininum quantity of posts needed to show the widget: Mo Modal Window Form Model type: More New Item News & Stories Newsletter widget No itens provided ;( No terms found to show! Sorry ;( Not found Not found in Trash November Number of terms: October Oldest Oldests One column One column wide Oops! That page can&rsquo;t be found. Open image in new tab? Open links in new tab? Page Page without sidebar Pagination style Please, fill all the fields. Post Type General NameProjects Post Type Singular NameProject Post excerpt Project Projects Radio Podcast Recommended Articles Related Posts Related posts Remove featured image Replace image Republish Resource Resource type sidebar Resources Resources for land and rights defense Resources sidebar Sa Search Search Item Section title See all  See more See more videos Select taxonomy September Set featured image Share Showing Showing <strong>%d</strong> of <strong>%d</strong> results Site by Sort by: Su Subject Subtitle Subtitle: Taxonomy Terms Taxonomy: Team Text Th Thematic cycle Themes There are no comments yet. Leave a comment! Title Title archiveResources for Land and Rights Defense Title: To Topics Tu Twitter Type Type here your search... Updated on Upload a Tool VIEW PAST EDITIONS Vertical Video Videos about the amazon View Item View Items Vlog Series Vlog sidebar We What are you looking for? Whatsapp You are not allowed to add HTML in any of those fields You must select a media partner to use as original publisher name by by:  card cards format:  https://newspack.blog j F Y \a\t G:i list of p placeholderSearch results show taxonomy vertical Project-Id-Version: JEO Theme 1.2.1
Report-Msgid-Bugs-To: https://wordpress.org/support/theme/jeo-theme
Last-Translator: 
Language-Team: Portuguese (Brazil)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2021-03-31T12:04:56+00:00
PO-Revision-Date: 2021-12-13 17:45+0000
X-Generator: Loco https://localise.biz/
X-Domain: jeo
Language: pt_BR
Plural-Forms: nplurals=2; plural=n != 1;
X-Loco-Version: 2.5.3; wp-5.8.2  -   e  %s comentários encontrados. Veja os comentários , ,  1 comentário encontrado. Veja o comentário Sobre o autor Sobre o escritor Academia Acesse a página do projeto Adicionar novo Adicionar novo item Adicionar CSS customizado Adicionar novo artigo Adicionar novo flyer Adicionar nova seção Adicionar link de republicado Adicionar link de republicado: Adicionar conteúdo: Adicionar informação Busca Avançada Todos Todos os itens Todos os episódios  Todas séries Todos vídeos Aplicar Abril Rádio Podcast Agosto Autor Biografia dos autores Biografia dos autores Listagem de autores Listagem de autores: Voltar para home Widget de marcador
 Marcadores:
 URL do botão:
 Botão de texto:
 Por Ao desabilitar isso, o cabeçalho usará o fundo padrão com alguma opacidade aplicada.
 Cancelar Cartas com fundo
 Estudos de caso Estudos de caso Estudo de caso Peso de categoria Marque para ativar o "desculpe, dissemos errado":
 Escolha um idioma: Limpar Clique neste botão para carregar mais resultados Fechar busca Contato Shortcode do formulário de contato: Barra lateral do modelo de contato
 Estilo do contêiner:
 Título da caixa de conteúdo
 Opções de conteúdo
 Copiar Logotipo de direitos autorais
 País Personalizada Logotipo escuro a ser exibido. O logotipo do modo escuro padrão é o seu logotipo mascarado com branco.
 Data Peso de data Dezembro Cor do marcador de decoração
 Estilo de marcador de decoração
 Padrão Descrição: Descoberta Estilo do botão Discovery
 Editar item Erro Exemplo: Dosis
 Exemplo: Libre Basquesville
 Exemplo: Open Sans Condensed. Se estiver vazio, a fonte do cabeçalho será usada.
 Facebook Imagem em destaque
 Slider em destaque
 Episódios destacados Configuração da imagem apresentada:
 Fevereiro Filtre a lista dos itens Filtre a lista Lista de flyers Siga nossos canais Siga nossos canais: Classe de ícone impressionante da fonte:
 Tamanho de fonte Tamanhos de fonte Tamanhos de fonte para celular
 Force o tamanho da fonte usando! Important
 Formato Sex De Faça Parte Área Faça Parte H1 H2 H3 H4 H5 H6 Hacklab Ocultar resumo do post Ocultar resumo em todos os posts Ocultar resumo do post: Horizontal Se a opção genérica para ocultar postagens no painel do personalizador estiver habilitada, ela substituirá esta opção
 Galeria de imagens
 Iniciativa de
 Inserir no item
 Parece que não encontramos nada com esses critérios ;( Arquivos de itens
 Atributos do item
 Lista de itens
 Navegação na lista de itens
 Tema JEO Janeiro Julho Junho SAIBA MAIS Conhecimento Intercâmbio de conhecimento Intercâmbio de conhecimento Sidebar de intercâmbio de conhecimento Sidebar de tipo de intercâmbio de conhecimento Idioma Link da última edição: Mais recente Últimos episódios Mais recente Assunto Deixe um comentário Navegação na lista de itens
 Deixe em branco para usar a cor primária
 Deixe em branco para se esconder.
 Linha Lista suspensa de links
 Carregar mais Link de login para academia
 Imagem escura do logotipo
 Imagem pegajosa do logotipo
 DD/MM/AAAA MAIS Email Mapas Março Imagem de fundo do marcador
 Quantidade máxima de postagens mostradas no widget:
 Maio Link de parceiro de mídia (funciona com parceiros de mídia selecionados)
 Tamanho da fonte do menu (rem)
 Quantidade mínima de postagens necessárias para mostrar o widget:
 Seg Formulário de janela modal
 Tipo de modelo: Mais Novo item Notícias e Histórias Widget de Newsletter Nenhum item foi fornecido ;( Nenhum termo foi encontrado para ser exibido! Desculpe Não encontrado Não encontrado na lixeira Novembro Quantidade de termos: Outubro Mais antigo Mais antigo Uma coluna Uma coluna ampla Oops! Essa página não pode ser encontrada. Abrir imagem em uma nova aba? Abrir links em uma nova aba? Página Página sem barra lateral Estilo da paginação Por favor, preencha todos os campos Projeots Projeto Resumo do post Projeto Projetos Rádio Podcast Artigos recomendados Posts relacionados Posts relacionados Remover imagem destacada Substituir as imagens Republicar Recurso Sidebar do tipo de recurso Recursos Recursos para defesa territorial e de direitos Sidebar dos recursos Sab Buscar Buscar item Título de seção  Ver todos Veja mais Veja mais vídeos Selecione uma taxonomia Setembro Definir imagem destacada Compartilhar Mostrando Mostrando <strong>%d</strong> de <strong>%d</strong> resultados Site por Ordenar por: Dom Assunto Subtítulo Subtítulo: Termos da taxonomia Taxonomia: Time: Texto Qui Ciclo temático Temas Não tem comentários ainda. Deixe um comentário! Título Recursos para a Defesa Territorial e de Direitos Título: Para Tópicos Ter Twitter Tipo Digite aqui sua busca... Atualizado em Carregar uma ferramenta Ver edições passadas Vertical Blogs & Vlogs Vídeos sobre a Amazônia Ver item Ver itens Vlog Series Barra lateral do Vlog Qua O que você procura? WhatsApp Você não tem permissão para adicionar HTML em qualquer um desses campos
 Você deve selecionar um parceiro de mídia para usar como nome do editor original
 por por:  cartão cartões formato: https://newspack.blog j F Y \à\s G:i lista de p Buscar resultados exibir taxonomia vertical 