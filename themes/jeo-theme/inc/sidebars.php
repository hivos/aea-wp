<?php
/**
 * Register our sidebars, widgetized areas and widgets.
 *
 */
function widgets_areas() {

	register_sidebar(array(
		'name'          => 'Article below author info',
		'id'            => 'after_post_widget_area',
		'before_widget' => '<div class="widget-area-after-post">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

	register_sidebar(array(
		'name'          => 'Category page sidebar',
		'id'            => 'category_page_sidebar',
		'before_widget' => '<div class="widget widget-category_page_sidebar">',
		'after_widget'  => '</div>',
		'before_title' => '<!--',
		'after_title' => '-->',
	));

	register_sidebar(array(
		'name'          => 'Search page sidebar',
		'id'            => 'search_page_sidebar',
		'before_widget' => '<div class="widget widget-category_page_sidebar">',
		'after_widget'  => '</div>',
		'before_title' => '<!--',
		'after_title' => '-->',
	));

	register_sidebar(array(
		'name'          => 'Author page sidebar',
		'id'            => 'author_page_sidebar',
		'before_widget' => '<div class="widget-author_page_sidebar">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="author_page_sidebar">',
		'after_title'   => '</h2>',
	));

	register_sidebar(array(
		'name'          => 'Republish modal bullets',
		'id'            => 'republish_modal_bullets',
		'before_widget' => '<div class="widget-area-after-post">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

	register_sidebar(array(
		'name'          => 'Project Archive Description',
		'id'            => 'project_archive_description',
		'before_widget' => '<div class="widget-project_archive_description">',
		'after_widget'  => '</div>',
		'before_title' => '<!--',
		'after_title' => '-->',
	));

    register_sidebar(array(
		'name'          => 'Knowledge Exchange Archive Description',
		'id'            => 'knowledge_exchange_archive_description',
		'before_widget' => '<div class="widget-area-knowledge_exchange_archive_description">',
		'after_widget'  => '</div>',
		'before_title' => '<!--',
		'after_title' => '-->',
	));

    register_sidebar(array(
		'name'          => 'Resource Archive Description',
		'id'            => 'resource_archive_description',
		'before_widget' => '<div class="widget-resource_archive_description">',
		'after_widget'  => '</div>',
		'before_title' => '<!--',
		'after_title' => '-->',
	));

    register_sidebar(array(
		'name'          => __("Vlog sidebar", "jeo"),
		'id'            => 'vlog_sidebar',
		'before_widget' => '<div class="widget widget-wrapper">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

    register_sidebar(array(
		'name'          => __("Podcast sidebar", "jeo"),
		'id'            => 'podcast_sidebar',
		'before_widget' => '<div class="widget widget-wrapper">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

    register_sidebar(array(
		'name'          => __("Resources sidebar", "jeo"),
		'id'            => 'resources_sidebar',
		'before_widget' => '<div class="widget widget-wrapper">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

    register_sidebar(array(
		'name'          => __("Resource type sidebar", "jeo"),
		'id'            => 'resource_type_sidebar',
		'before_widget' => '<div class="widget widget-wrapper">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

    register_sidebar(array(
		'name'          => __("Knowledge exchange sidebar", "jeo"),
		'id'            => 'knowledge_exchange_sidebar',
		'before_widget' => '<div class="widget widget-wrapper">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

    register_sidebar(array(
		'name'          => __("Knowledge exchange type sidebar", "jeo"),
		'id'            => 'knowledge_exchange_type_sidebar',
		'before_widget' => '<div class="widget widget-wrapper">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

    register_sidebar(array(
		'name'          => __("Contact template sidebar", "jeo"),
		'id'            => 'contact_sidebar',
		'before_widget' => '<div class="widget widget-wrapper">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

    register_sidebar(array(
		'name'          => __("Get involved area", "jeo"),
		'id'            => 'get_involved',
		'before_widget' => '<div class="widget widget-wrapper">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
}
add_action('widgets_init', 'widgets_areas');