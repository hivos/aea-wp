<?php

    $supported_types = [
        'resource', 'knowledge_exchange', 'post', 'case_study',
    ];

    $supported_types_toolbar = [
        'resource', 'knowledge_exchange',
    ];
    
    $matching_criterias_toolbar = [
        'resource' => [
            'taxonomy' => 'resource_type',
            'label' => __('Resource', 'jeo'),
            'label_toolbar' => __('Resources', 'jeo'),
            'icon' => '<i class="fas fa-book-reader"></i>',
        ],
    
        'knowledge_exchange' => [
            'taxonomy' => 'knowledge_exchange_type',
            'label' => __('Knowledge Exchange', 'jeo'),
            'label_toolbar' => __('Knowledge', 'jeo'),
            'icon' => '<i class="fas fa-leaf"></i>',
        ],
    ];

    $matching_criterias = [
        'resource' => [
            'taxonomy' => 'resource_type',
            'label' => __('Resource', 'jeo'),
            'label_toolbar' => __('Resources', 'jeo'),
            'icon' => '<i class="fas fa-book-reader"></i>',
        ],
    
        'knowledge_exchange' => [
            'taxonomy' => 'knowledge_exchange_type',
            'label' => __('Knowledge Exchange', 'jeo'),
            'label_toolbar' => __('Knowledge', 'jeo'),
            'icon' => '<i class="fas fa-leaf"></i>',
        ],
        
        'post' => [
            'taxonomy' => 'category',
            'label' => __('News & Stories', 'jeo'),
        ],
    
        'case_study' => [
            'taxonomy' => '',
            'label' => __('Case studies', 'jeo'),
            'label_toolbar' => __('Case studies', 'jeo'),
            'icon' => '<i class="fas fa-map"></i>',
        ],
    ];

    /* Used in Search Page */
    $post_type_labels = [
        'post' => __('News & Stories', 'jeo'),
        'video' => __('Video', 'jeo'),
        'audio' => __('Audio', 'jeo'),
        'page' => __('Page', 'jeo'),
        'case_study' => __('Case study', 'jeo'),
        'knowledge_exchange' => __('Knowledge exchange', 'jeo'),
        'resource' => __('Resource', 'jeo'), 
    ];

?>





