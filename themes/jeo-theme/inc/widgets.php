<?php

// Creates  widgets
class newsletter_widget extends WP_Widget {

	// The construct part
	function __construct() {
		parent::__construct(
			'newsletter_widget',
			__('Newsletter', 'newsletter_widget_domain'),
			array('description' => __('Newsletter widget', 'jeo'),)
		);
	}

	public function widget($args, $instance) {
?>
	<?php if ($instance) : ?>
		<div class="widget widget-wrapper">
			<div class="newsletter <?= _e($instance['model_type'], 'jeo') ?> <?= $instance['custom_style'] ?>">
				<?= ($instance['model_type'] == 'horizontal') ? '<div>' : '' ?>
				<div class="newsletter-header">
				    <i class="fa fa-envelope fa-3x" aria-hidden="true"></i>
					<p><?= _e($instance['title'], 'jeo') ?></p>
				</div>
				<p class="anchor-text">
				<?= _e($instance['subtitle'], 'jeo') ?>
					<?php if (!empty($instance['last_edition_link']) && $instance['model_type'] == 'horizontal') : ?>
						<?= empty($instance['last_edition_link']) ? '' :  '<a href="' . $instance['last_edition_link'] . '">' . __('VIEW PAST EDITIONS', 'jeo') . '</a>' ?>
					<?php endif; ?>
				</p>
				<?= ($instance['model_type'] == 'horizontal') ? '</div>' : '' ?>
				<?= ($instance['model_type'] == 'horizontal') ? '<div>' : '' ?>
				<?php if (!empty($instance['newsletter_shortcode'])) : ?>
					<?= do_shortcode($instance['newsletter_shortcode']) ?>
				<?php endif; ?>
				<?php if (!empty($instance['adicional_content'])) : ?>
					<p class="link-add"><?= _e($instance['adicional_content'], 'jeo') ?></p>
				<?php endif; ?>
				<?php if (!empty($instance['last_edition_link']) && $instance['model_type'] == 'vertical') : ?>
					<p class="last-edition"><?= empty($instance['last_edition_link']) ? '' :  '<a href="' . $instance['last_edition_link'] . '">'. __('VIEW PAST EDITIONS', 'jeo'). '</a>' ?></p>
				<?php endif; ?>
				<?= ($instance['model_type'] == 'horizontal') ? '</div>' : '' ?>
			</div>
		</div>
	<?php endif; ?>
	<?php
	}

	public function form($instance) {
		$title = !empty($instance['title']) ? $instance['title'] : esc_html__('', 'jeo');
		$subtitle = !empty($instance['subtitle']) ? $instance['subtitle'] : esc_html__('', 'jeo');
		$newsletter_shortcode = !empty($instance['newsletter_shortcode']) ? $instance['newsletter_shortcode'] : esc_html__('', 'jeo');
		$last_edition_link = !empty($instance['last_edition_link']) ? $instance['last_edition_link'] : esc_html__('', 'jeo');
		$adicional_content = !empty($instance['adicional_content']) ? $instance['adicional_content'] : esc_html__('', 'jeo');
		$model_type = !empty($instance['model_type']) ? $instance['model_type'] : esc_html__('vertical', 'jeo');
		$custom_style = !empty($instance['custom_style']) ? $instance['custom_style'] : esc_html__('', 'jeo');
	?>
		<p>
			<?= _e('You are not allowed to add HTML in any of those fields', 'jeo') ?>
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('model_type')); ?>"><?php esc_attr_e('Model type:', 'jeo'); ?></label>
			<select class="widefat" id="<?php echo esc_attr($this->get_field_id('model_type')); ?>" name="<?php echo esc_attr($this->get_field_name('model_type')); ?>">
				<option value="horizontal" <?= $model_type == 'horizontal' ? 'selected' : '' ?>><?php _e('Horizontal', 'jeo') ?></option>
				<option value="vertical" <?= $model_type == 'vertical' ? 'selected' : '' ?>><?php _e('Vertical', 'jeo') ?></option>
			</select>
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('subtitle')); ?>"><?php esc_attr_e('Subtitle:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('subtitle')); ?>" name="<?php echo esc_attr($this->get_field_name('subtitle')); ?>" type="text" value="<?php echo esc_attr($subtitle); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('newsletter_shortcode')); ?>"><?php esc_attr_e('Newsletter form shortcode:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('newsletter_shortcode')); ?>" name="<?php echo esc_attr($this->get_field_name('newsletter_shortcode')); ?>" type="text" value="<?php echo esc_attr($newsletter_shortcode); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('last_edition_link')); ?>"><?php esc_attr_e('Last edition link:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('last_edition_link')); ?>" name="<?php echo esc_attr($this->get_field_name('last_edition_link')); ?>" type="text" value="<?php echo esc_attr($last_edition_link); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('adicional_content')); ?>"><?php esc_attr_e('Additional Content:', 'jeo'); ?></label>
			<textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('adicional_content')); ?>" name="<?php echo esc_attr($this->get_field_name('adicional_content')); ?>"><?php echo $adicional_content; ?></textarea>
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('custom_style')); ?>"><?php esc_attr_e('Container style:', 'jeo'); ?></label>
			<textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('custom_style')); ?>" name="<?php echo esc_attr($this->get_field_name('custom_style')); ?>"><?php echo $custom_style; ?></textarea>
		</p>


	<?php
	}
}

// Academy login
class Academy_Login extends WP_Widget {

	// The construct part
	function __construct() {
		parent::__construct(
			'academy_widget',
			__('Academy', 'jeo'),
			array('description' => __('Login to academy link', 'jeo'),)
		);
	}

	public function widget($args, $instance) {
?>
	<?php if ($instance) : ?>
		<div class="widget widget-wrapper">
            <div class="login-to-academy-widget">
                <div class="academy-header">
                    <i class="fas fa-chalkboard-teacher"></i>
					<?= _e($instance['title'], 'jeo') ?>
				</div>

                <p class="anchor-text">
				    <?= _e($instance['subtitle'], 'jeo') ?>
				</p>

                <?php if(isset($instance['button_url']) && isset($instance['button_text'])): 
                    $words = explode(" ", $instance['button_text']);
                ?>
                <a id="academy-link" target="_blank" href="<?= $instance['button_url'] ?>">
                    <?php 
                        foreach($words as $word){
                            echo "<span>" . $word . " </span>";
                        }
                    ?>
                </a>
                <?php endif; ?>
            </div>
		</div>
	<?php endif; ?>
	<?php
	}

	public function form($instance) {
		$title = !empty($instance['title']) ? $instance['title'] : esc_html__('', 'jeo');
		$subtitle = !empty($instance['subtitle']) ? $instance['subtitle'] : esc_html__('', 'jeo');
		$button_text = !empty($instance['button_text']) ? $instance['button_text'] : esc_html__('', 'jeo');
        $button_url = !empty($instance['button_url']) ? $instance['button_url'] : esc_html__('', 'jeo');
	?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('subtitle')); ?>"><?php esc_attr_e('Subtitle:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('subtitle')); ?>" name="<?php echo esc_attr($this->get_field_name('subtitle')); ?>" type="text" value="<?php echo esc_attr($subtitle); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('button_text')); ?>"><?php esc_attr_e('Button text:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('button_text')); ?>" name="<?php echo esc_attr($this->get_field_name('button_text')); ?>" type="text" value="<?php echo esc_attr($button_text); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('button_url')); ?>"><?php esc_attr_e('Button URL:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('button_url')); ?>" name="<?php echo esc_attr($this->get_field_name('button_url')); ?>" type="text" value="<?php echo esc_attr($button_url); ?>">
		</p>
	<?php
	}
}

function academy_load_widget() {
	register_widget('Academy_Login');
}

add_action( 'widgets_init', 'academy_load_widget' );


// Creates  widgets
class modal_form_widget extends WP_Widget {
    public $widget_args = null;
    public $widget_instance = null;
    public $modal_id = null;

	// The construct part
	public function __construct() {
		parent::__construct(
			'modal_form_widget',
			__('Modal Window Form', 'jeo'),
			array('description' => __('Modal Window Form', 'jeo'),)
		);

        add_action('admin_footer', array( $this, 'add_admin_scripts' ), 99 );
        add_action('customize_controls_print_scripts', array( $this, 'add_admin_scripts' ), 99 );

	}

    public function add_admin_scripts() {
        echo "<script>
        jQuery(document).ready(function ($) {
            function media_upload(button_selector) {
              var _custom_media = true,
                  _orig_send_attachment = wp.media.editor.send.attachment;
              $('body').on('click', button_selector, function () {
                var button_id = $(this).attr('id');
                wp.media.editor.send.attachment = function (props, attachment) {
                  if (_custom_media) {
                    $('.' + button_id + '_img').attr('src', attachment.url);
                    $('.' + button_id + '_url').val(attachment.url);
                  } else {
                    return _orig_send_attachment.apply($('#' + button_id), [props, attachment]);
                  }
                }
                wp.media.editor.open($('#' + button_id));
                return false;
              });
            }
            media_upload('.js_custom_upload_media');
          });
        </script>";

    }
	public function widget($args, $instance) {
        $this->widget_args = $args; 
        $this->widget_instance = $instance;
		$this->modal_id = str_shuffle( 'modal_id0001' );
        $icon_class = !empty($this->widget_instance['icon_class']) ? $this->widget_instance['icon_class'] : 'fas fa-book-reader';
		$btn_description = !empty($instance['btn_description']) ? $instance['btn_description'] : esc_html__('Upload a Tool', 'jeo');

        wp_enqueue_script ( 'widget-modal-form', get_stylesheet_directory_uri() . '/assets/javascript/widget-modal-form.js' );

        $image = !empty($instance['image_uri']) ? $instance['image_uri'] : esc_html__('', 'jeo');
        $style = '';
        if ( $image && ! empty( $image ) ) {
            $style = "background:url($image);";
        }
        //echo 'cheguei aqui 1';
        $this->load_form_html();
        //add_action( 'wp_footer', array( &$this, 'load_form_html'), 9999999 );
?>
	<?php if ($instance) : ?>
		<div class="widget widget-wrapper">
			<div class="modal-form-widget vertical" style="<?php echo $style;?>">
				<div class="newsletter-header">
				    <i class="<?php echo $icon_class;?>" aria-hidden="true"></i>
					<p><?= _e($instance['title'], 'jeo') ?></p>
				</div>
				<p class="anchor-text">
				<?= _e($instance['subtitle'], 'jeo') ?>
				</p>
				<?php if (!empty($instance['adicional_content'])) : ?>
					<p class="link-add"><?= _e($instance['adicional_content'], 'jeo') ?></p>
				<?php endif; ?>
				<div class="wp-block-button wp-block-button__link--shadow">
					<a class="modal-open" data-modal-id="<?php echo $this->modal_id;?>">
						<?php echo $btn_description;?>
					</a>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php
	}

    /** 
     * Load modal window HTML and hidden it
     */
    public function load_form_html() {

        $icon_class = !empty($this->widget_instance['icon_class']) ? $this->widget_instance['icon_class'] : 'fas fa-book-reader';

        echo "<div id='$this->modal_id-bg' style='display:none;' class='modal-form-window-bg'>";
            echo "<div id='$this->modal_id-content' class='modal-form-window-content'>"; 
            ?>
            <a class="modal-close-icon" data-modal-id="<?php echo $this->modal_id;?>">
                <i class="fas fa-times"></i>
            </a>
            <div class="newsletter-header">
                <div class="modal-header-grid">
                    <i class="<?php echo $icon_class;?>" aria-hidden="true"></i>
                    <p><?php _e($this->widget_instance['title'], 'jeo') ?></p>
                </div>
            </div>
            <p class="anchor-text">
                <?php _e($this->widget_instance['subtitle'], 'jeo') ?>
            </p>
        <?php
            if ( !empty( $this->widget_instance['newsletter_shortcode'] ) ) {
                echo do_shortcode( $this->widget_instance['newsletter_shortcode'] );
            }
            echo "</div>";
        echo "</div>";
    }
	public function form($instance) {
		$title = !empty($instance['title']) ? $instance['title'] : esc_html__('', 'jeo');
        $icon_class = !empty($instance['icon_class']) ? $instance['icon_class'] : 'fas fa-book-reader';
		$subtitle = !empty($instance['subtitle']) ? $instance['subtitle'] : esc_html__('', 'jeo');
		$newsletter_shortcode = !empty($instance['newsletter_shortcode']) ? $instance['newsletter_shortcode'] : esc_html__('', 'jeo');

        $image = !empty($instance['image_uri']) ? $instance['image_uri'] : esc_html__('', 'jeo');
		$btn_description = !empty($instance['btn_description']) ? $instance['btn_description'] : esc_html__('Upload a Tool', 'jeo');


	?>
		<p>
			<?= _e('You are not allowed to add HTML in any of those fields', 'jeo') ?>
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
		</p>
        <p>
			<label for="<?php echo esc_attr($this->get_field_id('icon_class')); ?>"><?php esc_attr_e('Font Awesome Icon Class:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('icon_class')); ?>" name="<?php echo esc_attr($this->get_field_name('icon_class')); ?>" type="text" value="<?php echo esc_attr($icon_class); ?>">
		</p>
        <p>
            <label for="<?= $this->get_field_id( 'image_uri' ); ?>">Background Image</label>
            <img class="<?= $this->id ?>_img" src="<?= (!empty($instance['image_uri'])) ? $instance['image_uri'] : ''; ?>" style="margin:0;padding:0;max-width:100%;display:block"/>
            <input type="text" class="widefat <?= $this->id ?>_url" name="<?= $this->get_field_name( 'image_uri' ); ?>" value="<?= $instance['image_uri'] ?? ''; ?>" style="margin-top:5px;" />
            <input type="button" id="<?= $this->id ?>" class="button button-primary js_custom_upload_media" value="Upload Image" style="margin-top:5px;" />
        </p>


		<p>
			<label for="<?php echo esc_attr($this->get_field_id('subtitle')); ?>"><?php esc_attr_e('Subtitle:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('subtitle')); ?>" name="<?php echo esc_attr($this->get_field_name('subtitle')); ?>" type="text" value="<?php echo esc_attr($subtitle); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('newsletter_shortcode')); ?>"><?php esc_attr_e('Contact form shortcode:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('newsletter_shortcode')); ?>" name="<?php echo esc_attr($this->get_field_name('newsletter_shortcode')); ?>" type="text" value="<?php echo esc_attr($newsletter_shortcode); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('btn_description')); ?>"><?php esc_attr_e('Open Modal Button Description:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('btn_description')); ?>" name="<?php echo esc_attr($this->get_field_name('btn_description')); ?>" type="text" value="<?php echo esc_attr($btn_description); ?>">
		</p>

	<?php
	}
}

class bullet_widget extends WP_Widget {

	// The construct part
	function __construct() {
		parent::__construct(
			'bullet_widget',
			__('Bullet', 'bullet_widget_domain'),
			array('description' => __('Bullet widget', 'jeo'),)
		);
	}

	public function widget($args, $instance) {
?>
	<?php if ($instance) : ?>
	<?php endif; ?>
	<?php
	}

	public function form($instance) {
		$title = !empty($instance['title']) ? $instance['title'] : esc_html__('', 'jeo');
		$description = !empty($instance['description']) ? $instance['description'] : esc_html__('', 'jeo');
	?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('description')); ?>"><?php esc_attr_e('Description:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('description')); ?>" name="<?php echo esc_attr($this->get_field_name('description')); ?>" type="textarea" value="<?php echo esc_attr($description); ?>">
		</p>
	<?php
	}
}

class most_read_widget extends WP_Widget {

	// The construct part
	function __construct() {
		parent::__construct(
			'most_read_widget',
			__('Most Read', 'most_read_widget_domain'),
			array('description' => __('Most Read Widget', 'jeo'),)
		);
	}

	public function widget($args, $instance) {
		$authors = get_coauthors();
		$authors_ids = [];
		$category = '';
		$tag = '';
		$author = '';
		$most_read = [];
		$ids = [];
		$posts_ids = [];
		$posts_query_args = [];

		if(is_category()) {
			global $wp_query;

			$category = $wp_query->get_queried_object();
			$most_read = \PageViews::get_top_viewed(-1, ['post_type' => 'post', 'from' => '01-01-2001']);
			$posts_query_args['category__in'] = [$category->term_id];
		} else if(is_tag()) {
			$tag = get_queried_object();
			$most_read = \PageViews::get_top_viewed(-1, ['post_type' => 'post', 'from' => '01-01-2001']);
			$posts_query_args['tag__in'] = [$tag->term_id];
		} else if(is_author()) {
			foreach($authors as $author) {
				array_push($authors_ids, $author->ID);
			}

			$most_read = \PageViews::get_top_viewed(-1, ['post_type' => 'post', 'from' => '01-01-2001']);
			$posts_query_args['author__in'] = $authors_ids;
			$posts_query_args['meta_query'] = [[
				'relation' => 'OR',
				['key' => 'author-bio-display', 'value' => 1],
				['key' => 'authors-listing', 'value' => 1],
			]];
		} else {
			$most_read = \PageViews::get_top_viewed(-1, ['post_type' => 'post', 'from' => '01-01-2001']);
		}

		$ids = array();
		foreach ($most_read as $post => $value) {
			array_push($ids, $value->post_id);
		}

		$posts_query_args['post__in'] = $ids;
		$posts_query_args['orderby'] = 'post__in';
		$most_read_query = new \WP_Query($posts_query_args);

		foreach ($most_read_query->posts as $post => $value) {
			array_push($posts_ids, $value->ID);
		}

		?>
			<?php if($instance): ?>
				<?php if(sizeof($posts_ids) >= $instance['min_posts']): ?>
					<div class="category-most-read">
						<div class="category-most-read__header">
							<p><?= $instance['title'] ?> </p>
						</div>
						<div class="posts">
							<?php foreach(array_slice($posts_ids, 0, $instance['max_posts']) as $key=>$value){
								$title = get_the_title($value);
								$author_id = get_post_field( 'post_author', $value );
								$author = get_the_author_meta('display_name', $author_id);
								$url = get_permalink($value);
								$date_format = get_option( 'date_format' );
								$date = get_the_date($date_format, $value);
							?>
								<div class="post">
									<a class="post-link" href="<?php echo $url; ?>">
										<?php if($instance['featured_image'] == 'show'): ?>
											<div class="post-thumbnail"><?php echo get_the_post_thumbnail($value); ?></div>
										<?php endif ?>
										<p class="post-title"><?php echo $title; ?></p>
										<p class="post-date"><?php echo $date; ?></p>
									</a>
								</div>
							<?php } ?>
						</div>
					<?php endif ?>

				</div>
			<?php endif ?>
		<?php
	}

	public function form($instance) {
		$title = !empty($instance['title']) ? $instance['title'] : esc_html__('', 'jeo');
		$min_posts = !empty($instance['min_posts']) ? $instance['min_posts'] : 1;
		$max_posts = !empty($instance['max_posts']) ? $instance['max_posts'] : 3;
		$featured_image = !empty($instance['featured_image']) ? $instance['featured_image'] : esc_html__('show', 'jeo');

	?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('min_posts')); ?>"><?php esc_attr_e('Mininum quantity of posts needed to show the widget:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('min_posts')); ?>" name="<?php echo esc_attr($this->get_field_name('min_posts')); ?>" type="number" value="<?php echo esc_attr($min_posts); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('max_posts')); ?>"><?php esc_attr_e('Maximum quantity of posts shown in the widget:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('max_posts')); ?>" name="<?php echo esc_attr($this->get_field_name('max_posts')); ?>" type="number" value="<?php echo esc_attr($max_posts); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('featured_image')); ?>"><?php esc_attr_e('Featured image setting:', 'jeo'); ?></label>
			<select class="widefat" id="<?php echo esc_attr($this->get_field_id('featured_image')); ?>" name="<?php echo esc_attr($this->get_field_name('featured_image')); ?>">
				<option value="show" <?= $featured_image == 'show' ? 'selected' : '' ?>>Show featured image</option>
				<option value="hide" <?= $featured_image == 'hide' ? 'selected' : '' ?>>Hide featured image</option>
			</select>
		</p>
	<?php
	}
}

function my_post_gallery_widget($output, $attr) {
    global $post;

    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }

    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => 'dl',
        'icontag' => 'dt',
        'captiontag' => 'dd',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'exclude' => ''
    ), $attr));

    $id = intval($id);
    if ('RAND' == $order) $orderby = 'none';

    if (!empty($include)) {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    }

    if (empty($attachments)) return '';

	$output .= "<div class=\"image-gallery\">";
	$output .= "<div class=\"image-gallery-header\"><p>";
	$output .= $attr['title'];
	$output .= "</p></div>";
    $output .= "<div class=\"image-gallery-content-block wp-block-gallery columns-3 is-cropped\"><div class=\"blocks-gallery-grid\">";

    foreach ($attachments as $id => $attachment) {
        $img = wp_get_attachment_image_src($id, 'full');

        $output .= "<div class=\"blocks-gallery-item\">\n";
        $output .= "<img src=\"{$img[0]}\"/>\n";
        $output .= "</div>\n";
    }

	$output .= "</div></div>\n";
	$output .= "<button><a target=\"blank\" href=\"";
	$output .= $attr['see_more_url'];
	$output .= "\">SEE MORE</a></button>\n";

	$output .= "</div>\n";


    return $output;
}

class Flyer_List extends WP_Widget {

	// The construct part
	function __construct() {
        // Add Widget scripts
        add_action('admin_enqueue_scripts', array($this, 'scripts'));

		parent::__construct(
			'flyer_list',
			__('Flyer list', 'jeo'),
			array('description' => __('', 'jeo'),)
		);
	}

    public function scripts() {
        wp_enqueue_script( 'media-upload' );
        wp_enqueue_media();
        wp_enqueue_script( 'flyer-editor', get_stylesheet_directory_uri() . '/dist/flyer-editor.js', array('jquery'));
    }

	public function widget($args, $instance) { ?>
		<div class="flyer-wrapper">
            <?php if(!empty($instance['title'])): ?>
			<div class="title">
				<h3>
					<?= $instance['title'] ?>
				</h3>
			</div>
            <?php endif; ?>
            
            <?php if(isset($instance['storage']) && !empty($instance['storage'])): ?>
			<div class="flyer-itens">
				<?php 
					$itens = json_decode($instance['storage'], true);
					foreach($itens as $item): ?>
						<div class="flyer-item" id="<?= $item['id']?>">
							<a href="<?= $item['link'] ?>" target="<?= isset($instance['new_tab']) && $instance['new_tab'] == "on"? "_blank" : "" ?>">
								<img loading="lazy" src="<?= $item['image'] ?>">
							</a>
						</div>
					<?php 
					endforeach;
				?>
			</div>
            <?php else: ?>
                <?= __("No itens provided ;(", "jeo") ?>
            <?php endif; ?>


            <div class="flyer-itens mobile slide-avaliable" data-total-slides="<?= sizeof($itens) ?>">
				<?php 
					$itens = json_decode($instance['storage'], true);
					foreach($itens as $item): ?>
						<div class="flyer-item" id="<?= $item['id']?>">
							<a href="<?= $item['link'] ?>" target="<?= isset($instance['new_tab']) && $instance['new_tab'] == "on"? "_blank" : "" ?>">
								<img loading="lazy" src="<?= $item['image'] ?>">
							</a>
						</div>
					<?php 
					endforeach;
				?>
			</div>
		</div>
        
    <?php
	}

	public function form($instance) {
		$title = !empty($instance['title']) ? $instance['title'] : '';
        $new_tab = !empty($instance['new_tab']) ? $instance['new_tab'] : '';
        $storage_input = !empty($instance['storage']) ? $instance['storage'] : '';
        ?>
        <style>
            .flyer-actions-buttons button:first-child {
                margin-right: 15px;
            }
            .flyer-actions-buttons button:first-child {
                margin-right: 15px;
            }
            #flyer-options-area {
                padding: 15px 20px;
                background: #8080800f;
            }

            .flyer-edit-area {
                margin-bottom: 30px;
            }

        </style>

        <div class="flyer-list-widget">
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:', 'jeo'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
            </p>

            <p>
                <label for="<?php echo esc_attr($this->get_field_id('new_tab')); ?>"><?php esc_attr_e('Open image in new tab?', 'jeo'); ?></label>
                <input class="widefat" type="checkbox" id="<?php echo esc_attr($this->get_field_id('new_tab')); ?>" name="<?php echo esc_attr($this->get_field_name('new_tab')); ?>" type="text" <?= checked( "on", $new_tab, false ) ?>>
            </p>

            <input class="storage-input display-none" type="hidden" id="<?php echo esc_attr($this->get_field_id('storage')); ?>" name="<?php echo esc_attr($this->get_field_name('storage')); ?>" type="text" value="<?php echo esc_attr($storage_input); ?>">
            <div id="flyer-options-area">
            
            </div>

            <p>
                <button class="upload_image_button button button-primary widefat" type="button">
                    <?= __("Add new flyer", "jeo") ?>
                </button>
            </p>
        </div>
	<?php
	}
}

function flyer_load_widget() {
	register_widget('Flyer_List');
}

class Event_List extends WP_Widget {

	// The construct part
	function __construct() {
        // Add Widget scripts
        add_action('admin_enqueue_scripts', array($this, 'scripts'));

		parent::__construct(
			'event_list',
			__('Recommended Articles', 'jeo'),
			array('description' => '',)
		);
	}

    public function scripts() {
        wp_enqueue_script( 'upcoming-events', get_stylesheet_directory_uri() . '/dist/upcoming-events-editor.js', array('jquery'));
    }

	public function widget($args, $instance) { ?>
		<div class="upcoming-events-wrapper">
            <?php if(!empty($instance['title'])): ?>
                <div class="title">
                    <h3>
                        <?= $instance['title'] ?>
                    </h3>
                </div>
            <?php endif; ?>

			<div class="events-itens">
				<?php 
					$itens = json_decode($instance['storage'], true);
					foreach($itens as $item): ?>
						<div class="event-item" id="<?= $item['id']?>">
							<a href="<?= $item['link'] ?>" target="<?= isset($instance['new_tab']) && $instance['new_tab'] == "on"? "_blank" : "" ?>">
                                <?= $item['title'] ?>
                            </a>

                            <div>
                                <span>
                                    <?= __("by", "jeo") ?>
                                </span>

                                <span>
                                    <?= $item['authors'] ?>
                                </span>
                            </div>
						</div>
					<?php 
					endforeach;
				?>
			</div>
		</div>
        
    <?php
	}

	public function form($instance) {
		$title = !empty($instance['title']) ? $instance['title'] : '';
        $new_tab = !empty($instance['new_tab']) ? $instance['new_tab'] : '';
        $storage_input = !empty($instance['storage']) ? $instance['storage'] : '';
        ?>
        
        <div class="events-list-widget">
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:', 'jeo'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
            </p>

            <p>
                <label for="<?php echo esc_attr($this->get_field_id('new_tab')); ?>"><?php esc_attr_e('Open links in new tab?', 'jeo'); ?></label>
                <input class="widefat" type="checkbox" id="<?php echo esc_attr($this->get_field_id('new_tab')); ?>" name="<?php echo esc_attr($this->get_field_name('new_tab')); ?>" type="text" <?= checked( "on", $new_tab, false ) ?>>
            </p>

            <input class="storage-input display-none" type="hidden" id="<?php echo esc_attr($this->get_field_id('storage')); ?>" name="<?php echo esc_attr($this->get_field_name('storage')); ?>" type="text" value="<?php echo esc_attr($storage_input); ?>">
            <div id="events-options-area">
            
            </div>

            <p>
                <button class="add_new_event_button button button-primary widefat" type="button">
                    <?= __("Add new article", "jeo") ?>
                </button>
            </p>
        </div>
	<?php
	}
}

function comming_events_load_widget() {
	register_widget('Event_List');
}

function newsletter_load_widget() {
	register_widget('newsletter_widget');
}

function most_read_load_widget() {
	register_widget('most_read_widget');
}

function bullet_load_widget() {
	register_widget('bullet_widget');
}

function modal_load_widget() {
	register_widget('modal_form_widget');
}

add_action( 'widgets_init', 'comming_events_load_widget' );
add_action( 'widgets_init', 'flyer_load_widget' );
add_action( 'widgets_init', 'newsletter_load_widget' );
add_action( 'widgets_init', 'most_read_load_widget' );
add_action( 'widgets_init', 'bullet_load_widget' );
add_action( 'widgets_init', 'modal_load_widget' );

add_filter('post_gallery', 'my_post_gallery_widget', 10, 2);


// IMAGE GALLERY FORM
function image_gallery_form( $widget, $return, $instance ) {

    if ( 'media_gallery' == $widget->id_base ) {

        $see_more_url = isset( $instance['see_more_url'] ) ? $instance['see_more_url'] : '';
        ?>
            <p>
                <label for="<?php echo $widget->get_field_id('see_more_url'); ?>">
                    <?php _e( 'See more URL (requires https://)', 'jeo' ); ?>
                </label>
                <input class="text" value="<?php echo $see_more_url ?>" type="text" id="<?php echo $widget->get_field_id('see_more_url'); ?>" name="<?php echo $widget->get_field_name('see_more_url'); ?>" />
            </p>
        <?php
    }
}

function widget_save_form( $instance, $new_instance ) {

	$instance['see_more_url'] = $new_instance['see_more_url'];
	return $instance;
}

add_filter('in_widget_form', 'image_gallery_form', 10, 3 );
add_filter( 'widget_update_callback', 'widget_save_form', 10, 2 );
add_action('widgets_init', 'newsletter_load_widget');
add_action('widgets_init', 'most_read_load_widget');
?>