<?php
/**
 * Image box
 */
function custom_image_block() {

	// automatically load dependencies and version
	$asset_file = include(get_stylesheet_directory() . '/dist/imageBlock.asset.php');

	wp_register_script(
		'custom-image-block-editor',
		get_stylesheet_directory_uri() . '/dist/imageBlock.js',
		$asset_file['dependencies'],
		$asset_file['version']
		//filemtime(get_stylesheet_directory() . '/dist/imageBlock.js')
	);

	wp_register_style(
		'custom-image-block-editor',
		get_stylesheet_directory_uri() . '/assets/javascript/blocks/imageBlock/imageBlock.css',
		[],
		filemtime(get_stylesheet_directory() . '/assets/javascript/blocks/imageBlock/imageBlock.css'),
	);

	// wp_register_style(
	// 	'custom-image-block-block',
	// 	get_stylesheet_directory_uri() . '/assets/javascript/blocks/imageBlock/style.css',
	// 	array(),
	// 	filemtime(get_stylesheet_directory() . '/assets/javascript/blocks/imageBlock/style.css'),
	// 	'all',
	// );

	register_block_type('jeo-theme/custom-image-block-editor', array(
		'editor_script' => 'custom-image-block-editor',
		'editor_style'  => 'custom-image-block-editor',
		// 'style'         => 'custom-image-block-block',
	));
}

add_action('init', 'custom_image_block');


function custom_pullquote_scripts() {
	wp_enqueue_script(
		'be-editor',
		get_stylesheet_directory_uri() . '/assets/javascript/blocks/pullquoteBlock/index.js',
		array('wp-blocks', 'wp-dom'),
		filemtime(get_stylesheet_directory() . '/assets/javascript/blocks/pullquoteBlock/index.js'),
		true
	);

	wp_enqueue_style(
		'custom-pullquote-block',
		get_stylesheet_directory_uri() . '/assets/javascript/blocks/pullquoteBlock/style.css',
		array(),
		filemtime(get_stylesheet_directory() . '/assets/javascript/blocks/pullquoteBlock/style.css'),
		'all',
	);
}
add_action('enqueue_block_editor_assets', 'custom_pullquote_scripts');


function custom_group_block_scripts() {
	wp_enqueue_script(
		'be-editor-group',
		get_stylesheet_directory_uri() . '/assets/javascript/blocks/groupBlock/index.js',
		array('wp-blocks', 'wp-dom'),
		filemtime(get_stylesheet_directory() . '/assets/javascript/blocks/groupBlock/index.js'),
		true
	);

	wp_enqueue_style(
		'custom-group-block',
		get_stylesheet_directory_uri() . '/assets/javascript/blocks/groupBlock/style.css',
		array(),
		filemtime(get_stylesheet_directory() . '/assets/javascript/blocks/groupBlock/style.css'),
		'all',
	);
}
add_action('enqueue_block_editor_assets', 'custom_group_block_scripts');


/* Heading */
function custom_heading_block_scripts() {
	wp_enqueue_script(
		'be-editor-heading',
		get_stylesheet_directory_uri() . '/assets/javascript/blocks/heading/index.js',
		array('wp-blocks', 'wp-dom'),
		filemtime(get_stylesheet_directory() . '/assets/javascript/blocks/heading/index.js'),
		true
	);

	wp_enqueue_style(
		'custom-heading-block',
		get_stylesheet_directory_uri() . '/assets/javascript/blocks/heading/style.css',
		array(),
		filemtime(get_stylesheet_directory() . '/assets/javascript/blocks/heading/style.css'),
		'all',
	);
}
add_action('enqueue_block_editor_assets', 'custom_heading_block_scripts');


/**
 * Newsletter
 */
function custom_newsletter_block() {

	// automatically load dependencies and version
	$asset_file = include(get_stylesheet_directory() . '/dist/newsletter.asset.php');

	wp_register_script(
		'custom-newsletter-block',
		get_stylesheet_directory_uri() . '/dist/newsletter.js',
		$asset_file['dependencies'],
		$asset_file['version']
		//filemtime(get_stylesheet_directory() . '/dist/imageBlock.js')
	);

	wp_register_style(
		'custom-newsletter-block',
		get_stylesheet_directory_uri() . '/assets/javascript/blocks/newsletter/newsletter.css',
		[],
		filemtime(get_stylesheet_directory() . '/assets/javascript/blocks/newsletter/newsletter.css'),
		'all'
	);

	register_block_type('jeo-theme/custom-newsletter-block', array(
		'editor_script' => 'custom-newsletter-block',
		'editor_style'  => 'custom-newsletter-block',
		//'style'         => 'custom-newsletter-block',
	));
}

add_action('init', 'custom_newsletter_block');

/**
 * Link Dropdown
 */
function custom_link_dropdown() {

	// automatically load dependencies and version
	$asset_file = include(get_stylesheet_directory() . '/dist/linkDropdown.asset.php');

	wp_register_script(
		'custom-link-dropdown',
		get_stylesheet_directory_uri() . '/dist/linkDropdown.js',
		$asset_file['dependencies'],
		$asset_file['version']
		//filemtime(get_stylesheet_directory() . '/dist/imageBlock.js')
	);

	wp_register_style(
		'custom-link-dropdown',
		get_stylesheet_directory_uri() . '/assets/javascript/blocks/linkDropdown/linkDropdown.css',
		[],
		filemtime(get_stylesheet_directory() . '/assets/javascript/blocks/linkDropdown/linkDropdown.css'),
		'all'
	);

	register_block_type('jeo-theme/custom-link-dropdown', array(
		'editor_script' => 'custom-link-dropdown',
		'editor_style'  => 'custom-link-dropdown',
		// 'style'         => 'custom-link-dropdown',
	));
}

add_action('init', 'custom_link_dropdown');

function custom_team_block() {

	// automatically load dependencies and version
	$asset_file = include(get_stylesheet_directory() . '/dist/teamBlock.asset.php');

	wp_register_script(
		'custom-team-block',
		get_stylesheet_directory_uri() . '/dist/teamBlock.js',
		$asset_file['dependencies'],
		$asset_file['version']
		//filemtime(get_stylesheet_directory() . '/dist/imageBlock.js')
	);
	// wp_register_style(
	// 	'custom-team-block',
	// 	get_stylesheet_directory_uri() . '/assets/javascript/blocks/teamBlock/teamBlock.scss',
	// 	[],
	// 	filemtime(get_stylesheet_directory() . '/assets/javascript/blocks/teamBlock/teamBlock.scss'),
	// 	'all'
	// );

	register_block_type('jeo-theme/custom-team-block', array(
		'editor_script' => 'custom-team-block',
		// 'editor_style'  => 'custom-team-block',
	));
}

add_action('init', 'custom_team_block');


/**
 * Video gallery
 */
function custom_video_gallery() {

	// automatically load dependencies and version
	$asset_file = include(get_stylesheet_directory() . '/dist/videoGallery.asset.php');

	wp_register_script(
		'custom-video-gallery',
		get_stylesheet_directory_uri() . '/dist/videoGallery.js',
		$asset_file['dependencies'],
		$asset_file['version']
		//filemtime(get_stylesheet_directory() . '/dist/imageBlock.js')
	);

	wp_register_style(
		'custom-video-gallery',
		get_stylesheet_directory_uri() . '/assets/javascript/blocks/videoGallery/style.css',
		[],
		filemtime(get_stylesheet_directory() . '/assets/javascript/blocks/videoGallery/style.css'),
		'all'
	);

	register_block_type('jeo-theme/custom-video-gallery', array(
		'editor_script' => 'custom-video-gallery',
		'editor_style'  => 'custom-video-gallery',
		//'style'         => 'custom-newsletter-block',
	));
}

add_action('init', 'custom_video_gallery');


/**
 * Team member
 */
function team_member_block() {

	// automatically load dependencies and version
	$asset_file = include(get_stylesheet_directory() . '/dist/teamMember.asset.php');

	wp_register_script(
		'team-member',
		get_stylesheet_directory_uri() . '/dist/teamMember.js',
		$asset_file['dependencies'],
		$asset_file['version']
		//filemtime(get_stylesheet_directory() . '/dist/imageBlock.js')
	);

	wp_register_style(
		'team-member',
		get_stylesheet_directory_uri() . '/assets/javascript/blocks/teamMember/style.css',
		[],
		filemtime(get_stylesheet_directory() . '/assets/javascript/blocks/teamMember/style.css'),
		'all'
	);

	register_block_type('jeo-theme/team-member', array(
		'editor_script' => 'team-member',
		'editor_style'  => 'team-member',
		//'style'         => 'custom-newsletter-block',
	));
}

add_action('init', 'team_member_block');


/**
 * Embed template
 */
function custom_embed_template() {
	// automatically load dependencies and version
	$asset_file = include(get_stylesheet_directory() . '/dist/embedTemplate.asset.php');

	wp_register_script(
		'custom-embed-template',
		get_stylesheet_directory_uri() . '/dist/embedTemplate.js',
		$asset_file['dependencies'],
		$asset_file['version']
		//filemtime(get_stylesheet_directory() . '/dist/imageBlock.js')
	);

	wp_register_style(
		'custom-embed-template',
		get_stylesheet_directory_uri() . '/assets/javascript/blocks/embedTemplate/style.css',
		[],
		filemtime(get_stylesheet_directory() . '/assets/javascript/blocks/embedTemplate/style.css'),
		'all'
	);

	register_block_type('jeo-theme/embed-template', array(
		'editor_script' => 'custom-embed-template',
		'editor_style'  => 'custom-embed-template',
		//'style'         => 'custom-newsletter-block',
	));
}

add_action('init', 'custom_embed_template');


/**
 * Content box
 */
function content_box_template() {
	// automatically load dependencies and version
	$asset_file = include(get_stylesheet_directory() . '/dist/contentBox.asset.php');

	wp_register_script(
		'content-box',
		get_stylesheet_directory_uri() . '/dist/contentBox.js',
		$asset_file['dependencies'],
		$asset_file['version']
		//filemtime(get_stylesheet_directory() . '/dist/imageBlock.js')
	);

	wp_register_style(
		'content-box',
		get_stylesheet_directory_uri() . '/assets/javascript/blocks/contentBox/style.css',
		[],
		filemtime(get_stylesheet_directory() . '/assets/javascript/blocks/contentBox/style.css'),
		'all'
	);

	register_block_type('jeo-theme/content-box', array(
		'render_callback' => 'content_box_render_callback',
		'editor_script' => 'content-box',
		'editor_style'  => 'content-box',
	));
}

function content_box_render_callback($block_attributes, $content) {
	$final_result = '<div class="content-box">';
	$final_result .= '	'. $content;
	$final_result .= '</div>';

	return $final_result;
}

add_action('init', 'content_box_template');

/**
 * Image Gallery
 */
function custom_image_gallery_block() {

	// automatically load dependencies and version
	$asset_file = include(get_stylesheet_directory() . '/dist/imageGallery.asset.php');

	wp_register_script(
		'custom-image-gallery-block',
		get_stylesheet_directory_uri() . '/dist/imageGallery.js',
		$asset_file['dependencies'],
		$asset_file['version']
		//filemtime(get_stylesheet_directory() . '/dist/imageBlock.js')
	);

	wp_register_style(
		'custom-image-gallery-block',
		get_stylesheet_directory_uri() . '/assets/javascript/blocks/imageGallery/dashboard.css',
		[],
		filemtime(get_stylesheet_directory() . '/assets/javascript/blocks/imageGallery/dashboard.css'),
		'all'
	);


	register_block_type('jeo-theme/custom-image-gallery-block', array(
		'editor_script' => 'custom-image-gallery-block',
		'editor_style'  => 'custom-image-gallery-block',
	));
}

add_action('init', 'custom_image_gallery_block');

/**
 * Featured Slider
 */
function featured_slider_block() {

	// automatically load dependencies and version
	$asset_file = include(get_stylesheet_directory() . '/dist/featuredSliderBlock.asset.php');

	wp_register_script(
		'featured-slider-block',
		get_stylesheet_directory_uri() . '/dist/featuredSliderBlock.js',
		$asset_file['dependencies'],
		$asset_file['version']
		//filemtime(get_stylesheet_directory() . '/dist/imageBlock.js')
	);

	wp_register_style(
		'featured-slider-block',
		get_stylesheet_directory_uri() . '/assets/javascript/blocks/featuredSlider/dashboard.css',
		[],
		filemtime(get_stylesheet_directory() . '/assets/javascript/blocks/featuredSlider/dashboard.css'),
		'all'
	);


	register_block_type('jeo-theme/featured-slider', array(
		'editor_script' => 'featured-slider-block',
		'editor_style'  => 'featured-slider-block',
	));
}

add_action('init', 'featured_slider_block');

/**
 * Toolbar tooltip
 */

add_action('enqueue_block_editor_assets', 'toolbar_tooltip');

function toolbar_tooltip() {
	wp_enqueue_script(
		'toolbar-tooltip',
		get_stylesheet_directory_uri() . '/dist/tooltip.js',
		array( 'wp-compose', 'wp-data', 'wp-blocks', 'wp-element', 'wp-components', 'wp-editor' ),
		'',
		true
	);

	wp_enqueue_style(
		'text-highlight-button-editor-css',
		get_stylesheet_directory_uri() . '/assets/javascript/toolbar/tooltip/dashboard.css',
		array( 'wp-edit-blocks' )
	);
}


/**
 * Register the block Taxonomy Terms
 */
function list_taxonomy_terms_block() {

    // load dependencies and version
    $asset_file = include( get_stylesheet_directory() . '/dist/taxonomyTerms.asset.php' );

    wp_register_script(
        'taxonomy-terms',
        get_stylesheet_directory_uri() . '/dist/taxonomyTerms.js',
        $asset_file['dependencies'],
        $asset_file['version']
        //filemtime(get_stylesheet_directory() . '/dist/imageBlock.js')
    );

    $post_types = get_post_types(
        array(
            'public'       => true,
            'show_in_rest' => true
        )
    );

    $taxonomies = get_object_taxonomies( $post_types, 'objects' );

    wp_localize_script(
        'taxonomy-terms',
        'useful_info',
        array(
            'post_types'    => $post_types,
            'taxonomy_list' => $taxonomies,
        )
    );

    register_block_type( 'jeo-theme/taxonomy-terms', array(
        'editor_script'   => 'taxonomy-terms',
        'render_callback' => 'taxonomy_terms_render',
        'attributes'      => [
            'className' => [
                'type' => 'string'
            ],
            'taxonomy' => [
                'type' => 'string'
            ],
            'number' => [
                'default' => '0',
                'type'    => 'string'
            ]
        ]
		// 'editor_style'  => 'custom-video-gallery',
		//'style'         => 'custom-newsletter-block',
	));
}

add_action( 'init', 'list_taxonomy_terms_block', 13 );

/**
 * Render the block Taxonomy Terms
 */
function taxonomy_terms_render( $attributes ) {

    $taxonomy = ( isset( $attributes['taxonomy'] ) ) ? $attributes['taxonomy'] : 'category';
    $number   = ( isset( $attributes['number'] ) ) ? $attributes['number'] : '0';
    $style    = ( isset( $attributes['className'] ) ) ? $attributes['className'] : '';

    $terms = get_terms(
        [
            'taxonomy'   => $taxonomy,
            'hide_empty' => false,
            'number'     => (int) $number
        ]
    );

    $args = [
        'taxonomy'   => $taxonomy,
        'hide_empty' => true,
        'number'     => (int) $number,
    ];

    /**
     * Check if $taxonomy has term_meta `date` registered and add meta_key parameter 
     */
    if ( $terms && ! is_wp_error( $terms ) && metadata_exists( 'term', $terms[0]->term_id, 'date' ) ) {

        $args['meta_key'] = 'date';
        $args['orderby']  = 'meta_value';
        $args['order']    = 'DESC';

    }

    /**
     * Check if $taxonomy has term_meta `order` registered and add meta_query parameter 
     */
    if ( $terms && ! is_wp_error( $terms ) && metadata_exists( 'term', $terms[0]->term_id, 'order' ) ) {

        $args['meta_query'] = [
            'key'  => 'order',
            'type' => 'NUMERIC'
        ];

    }

    $terms = get_terms( $args );

    ob_start();

    set_query_var( 'block_params', ['taxonomy' => $taxonomy, 'terms' => $terms, 'style' => $style] );
    get_template_part( 'template-parts/content/taxonomy', 'terms' );

    $output = ob_get_clean();

    return $output;
}

/**
 * Filters the meta_key used for the featured image of the `podcast_serie` taxonomy term
 */
function podcast_serie_featured_image_meta_key( $value, $taxonomy ) {

    if ( $taxonomy == 'podcast_serie' ) {
        return 'thumbnail';
    }

    return $value;
}

add_filter( 'block_taxonomy_terms_featured_image_meta_key', 'podcast_serie_featured_image_meta_key', 10, 2 );