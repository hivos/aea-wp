<?php 

function video_type_template() {
    $post_type_object = get_post_type_object( 'video' );
    
    if($post_type_object) {
        $post_type_object->template = array(
            array( 'core-embed/youtube', array('placeholder' => '')),
        );
        $post_type_object->template_lock = 'all';
    }
}

add_action( 'init', 'video_type_template', 12);
