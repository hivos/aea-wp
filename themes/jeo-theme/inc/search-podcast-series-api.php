<?php

    add_action( 'rest_api_init', function () {
        register_rest_route( 'podcast/v1', '/podcast_serie', [
                'methods' => 'GET',
                'callback' => 'search_podcast_series',
                'permission_callback' => function() {
                    return true;
                }
        ]);
    });

    function get_audios( $params ){
        
        $args = [
            'post_type' => 'audio',
            'posts_per_page' => -1,
            'suppress_filters' => false,
            'paged' => isset($params['page']) ? $params['page'] : 1,
        ];
       
        if( isset($params["tags"]) ){
            $args['tax_query'] = [
                [
                    'taxonomy'  => 'post_tag',
                    'field'     => 'id',
                    'terms'     => array_map('intval', explode(",", $params["tags"]))
                ]
            ];
        }

        if( isset($params["before"]) && isset($params["after"]) ){
            $args['date_query'] = [
                [
                    'before'    => $params["before"],
                    'after'     => $params["after"],
                    'inclusive' => true,
                ]
            ];
        }

        if( isset($params["aauthor"]) ){
            $args['tax_query'] = [
                [
                    'taxonomy'  => 'aauthor',
                    'field'     => 'id',
                    'terms'     => array_map( 'intval', explode(",", $params["aauthor"]) )
                ]
            ];
        }

        return new WP_Query($args);
    }

    function search_podcast_series( WP_REST_Request $request )
    {
        $series = [];
        $per_page = 6;
        $page = 1;
        $addedSeries = [];
        $params = $request->get_params();
        $audios = get_audios( $params );
       
        if( empty($audios) )
            return new WP_Error( 'no_posts', __('No post found'), [ 'status' => 404 ] );
        
        foreach($audios->posts as $audio){
            $terms = get_the_terms($audio->ID, 'podcast_serie');
            $name = $terms[0]->name;

            if(!in_array($name, $addedSeries)){
                $id = $terms[0]->term_id;
                $link = get_term_link($id);
                $meta = get_term_meta($id);
                $thumbUrl = isset($meta["thumbnail"]) ? wp_get_attachment_url(intval($meta["thumbnail"][0])) : "";
      
                $date = isset($meta["date"]) ? $meta["date"][0] : "";

                $serie = [
                    'id' => $id,
                    'name' => $name,
                    'link' => $link,
                    'date' => $date,
                    'thumbnail' => [
                        'guid' => $thumbUrl
                    ],
                ];
                
                 array_push($series, $serie); 
                 array_push($addedSeries, $name);
            }
        }   
        
        $total = count($series);
        $max_pages = $total/$per_page;
        
        $page = isset($params['page']) ? $params['page'] : 1;
        $series = paginate($series, $per_page, $page);
       
        $response = new WP_REST_Response($series, 200);
        $response->header( 'X-WP-Total', $total ); 
        $response->header( 'X-WP-TotalPages', ceil( $max_pages ) );

        return $response;
    }


    function paginate($data, $per_page, $page){

        $lastPos = $per_page * $page;
        $firstPos = $lastPos - $per_page;

        return  array_slice($data, $firstPos, $lastPos);
    }