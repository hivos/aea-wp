<?php
/**
 * Template Name: Vlog Series
 */
get_header();
$image_url = get_the_post_thumbnail_url();
?>
<header class="page-header" style="--term-image: url( '<?= $image_url ?>' )">
    <div class="wrapper">
        <h3 class="taxonomy-title">
            <?= __( 'Videos about the amazon', 'jeo' ) ?>
        </h3>
        <div class="term-title">
            <h1>
                <i class="fab fa-youtube"></i>
                <?php the_title(); ?>
            </h1>
        </div>

        <div class="term-description">
            <?= strip_tags(get_the_content(), "<p>"); ?>
        </div>
    </div><!-- /.wrapper -->
</header><!-- /.page-header -->

<header class="mobile-only">
    <div class="wrapper">
        <div class="term-description">
            <?= strip_tags(get_the_content(), "<p>"); ?>
        </div>
    </div>
</header><!-- /.mobile-only -->

<section id="primary" class="content-area custom-archive">

    <?php do_action( 'before_archive_posts' ); ?>

    <main id="main" class="site-main">
        <header class="page-header">
            <span>
                <h1 class="page-title article-section-title category-header">
                    <?= __( 'All videos', 'jeo' ) ?>
                </h1>

                <div class="type-description">
                    <?php dynamic_sidebar( 'resource_archive_description' ) ?>
                </div>
            </span>
        </header><!-- /.page-header -->

        <section class="related-content">
            <div class="related-vlog-series">
                <?php
                if ( get_query_var( 'paged' ) ) {
                    $paged = get_query_var( 'paged' );
                } else if ( get_query_var( 'page' ) ) {
                    $paged = get_query_var( 'page' );
                } else {
                    $paged = 1;
                }

                $per_page        = get_option( 'posts_per_page' );
                $number_of_pages = count( get_terms( [
                    'taxonomy'   => 'vlog_serie',
                    'hide_empty' => true,
                    'meta_key'   => 'date'
                ] ) );

                $offset          = $per_page * ( $paged - 1 );

                $args = [
                    'taxonomy'   => 'vlog_serie',
                    'offset'     => $offset,
                    'number'     => $per_page,
                    'hide_empty' => true,
                    'meta_key'   => 'date',
                    'orderby'    => 'meta_value',
                    'order'      => 'DESC'
                ];

                $series = get_terms( $args );

                foreach ( $series as $serie ) :
                    get_template_part( 'template-part-vlog-series', '', ['serie' => $serie] );
                endforeach;

                $pages = $number_of_pages / $per_page;

                if ( ceil( $pages ) > $paged ) {
                    echo '<div class="vlog-series-loadmore">' . __( 'Load more', 'jeo' ) . '</div>';
                }

                ?>
            </div><!-- /.related-vlog-series -->
        </section><!-- /.related-content -->
    </main><!-- #main -->

</section><!-- #primary -->
<?php
get_footer();
