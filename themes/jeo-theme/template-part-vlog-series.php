<?php $serie = $args['serie']; ?>

<div class="related-term-group">
    <div class="video-gallery-block">
        <div class="video-gallery-wrapper">

        <?php
            // Build posts query
            $posts_array = get_posts( [
                'posts_per_page' => -1,
                'post_type'      => 'video',
                'tax_query'      => array(
                    array(
                        'taxonomy' => 'vlog_serie',
                        'field'    => 'term_id',
                        'terms'    => $serie->term_id,
                    )
                )
            ]);

            $posts_count = count( $posts_array );
            $btn_see_more = ( $posts_count == 1 ) ? __( 'See more', 'jeo' ) : __( 'See more videos', 'jeo' );

            ?>
            <div class="serie-info">
                <h3>
                    <a href="<?= get_term_link( $serie ) ?>"><?= $serie->name ?></a>
                </h3>
                <p><?= hl_the_excerpt( term_description( $serie->term_id ), 230 ); ?></p>
                <a class="btn" href="<?= get_term_link( $serie ) ?>"><?= $btn_see_more ?></a>
            </div>

            <?php
            foreach ( $posts_array as $key => $post ) :
                setup_postdata( $post );
                $aauthors = get_the_terms( $post, 'aauthor' );
                $author = [];

                if ( $aauthors ) {
                    foreach ( $aauthors as $aauthor ) {
                        $author[] = $aauthor->name;
                    }
                    $author = implode( ', ', $author );
                }
            ?>
                <div class="embed-template-block">
                    <p><?php the_title() ?></p>
                    <p><?= $author ? $author : '' ?> </p>
                    <?php the_content(); ?>
                    <div class="video-excerpt">
                        <div class="video-excerpt-content">
                            <?php the_excerpt(); ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <a class="btn only-mobile" href="<?= get_term_link( $serie ) ?>"><?= $btn_see_more ?></a>

    <?php
    wp_reset_postdata();

    // Use posts found to build video gallery (only the style and video toggle logic will be reused / innerblock strategy wont work)
    ?>
</div>