<?php

/**
 * Template Name: Podcast's page
 */

get_header();
    $image_url = get_the_post_thumbnail_url();
?>
        <header class="page-header" style="--term-image: url('<?= $image_url ?>')">
            <div class="wrapper">
                <h3 class="taxonomy-title">
                    <?=  __( 'Podcasts about the Amazon', 'jeo' ) ?>
                </h3>
                <div class="term-title">
                    <h1>
                        <i class="fa fa-headphones"></i>
                        <?php the_title(); ?>
                    </h1>
                </div>
                                    
                <div class="term-description">
                    <?= strip_tags(get_the_content(), "<p>"); ?>
                </div>
                
            </div>
        </header><!-- .page-header -->

        <header class="mobile-only"> 
            <div class="wrapper">                    
                <div class="term-description">
                    <?= strip_tags(get_the_content(), "<p>"); ?>
                </div>
            </div>
        </header>

        
		<section id="primary" class="content-area custom-archive">
            
            <?php do_action( 'before_archive_posts' ); ?>
            
            <main id="main" class="site-main">
                <header>
                    <span>
                        <h1 class="page-title article-section-title category-header">
                            <?= __("Latest episodes", "jeo") ?>
                         </h1>
                                            
                        <div class="type-description">
                            <?php dynamic_sidebar('resource_archive_description') ?>
                        </div>
                    </span>
                </header>

                <section class="related-content">
                
                    <div class="related-vlog-series">
                        <?php
                            $args = [
                                'post_type'   => 'audio',
                                'posts_per_page'     => 2,
                            ];

                            $podcasts = new WP_Query( $args );
                        
                            while ( $podcasts->have_posts() ):
                                $podcasts->the_post(); ?>
                            <?= get_the_content(null, false, $post->ID); ?>
                        <?php
                            endwhile;
                            wp_reset_query();
                        ?>
                    </div>
                    <?php 
                        $filters = [
                            [
                                "type"           => "taxonomy",
                                "placeholder"    => __('Subject', 'jeo'),
                                "filterSettings" => [
                                    "taxonomy"          => 'post_tag',
                                    "multipleSelection" => true,
                                ]
                            ],
                            [   
                                "type"           => "taxonomy",
                                "placeholder"    => __('Author', 'jeo'),
                                "filterSettings" => [
                                    "taxonomy"          => 'aauthor',
                                    "multipleSelection" => true,
                                ] 
                            ],
                            [
                                "type"        => "date-range-picker",
                                "placeholder" => __('Date', 'jeo'),
                            ],
                        ];
    
                        $card_model = "PostCard";
                        $baseURL = get_rest_url( null, 'podcast/v1/podcast_serie' );
    
                        $data_building = [
                            'filters'            => $filters,
                            'cardModel'          => $card_model,
                            'baseURL'            => $baseURL,
                            'addicionalCardMeta' => [  
                                'type' => 'podcast',
                                'format' => 'vertical',
                            ],
                        ];
                    ?>
                
                    <h4><?= __("All series", "jeo") ?></h4>
    
                    <div id="faceted-posts-search" data-building="<?= htmlentities(json_encode($data_building)) ?>"></div>
                </section>
			</main><!-- #main -->
		</section><!-- #primary -->
<?php
get_footer();
;
