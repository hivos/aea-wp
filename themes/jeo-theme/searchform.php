<?php

/**
 * Template for displaying search forms.
 *
 * @package Newspack
 */

$unique_id = wp_unique_id('search-form-');

?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url(home_url('/')); ?>">
	<label for="<?php echo esc_attr($unique_id); ?>">
		<span class="screen-reader-text"><?php echo esc_html_x('Search for:', 'label', 'newspack'); ?></span>
	</label>
	<div class="search-input-wrapper">
		<input type="search" id="<?php echo esc_attr($unique_id); ?>" class="search-field" placeholder="<?php echo esc_attr_x('Search', 'placeholder', 'jeo'); ?>" value="<?php echo get_search_query(); ?>" name="s" />
		<button type="submit" class="search-submit">
			<?php echo wp_kses(newspack_get_icon_svg('search', 28), newspack_sanitize_svgs()); ?>
			<span class="screen-reader-text">
				<?php echo esc_html_x('Search', 'submit button', 'newspack'); ?>
			</span>
		</button>
	</div>

	<?php if ($args['aria_label'] == 'search-page-form') : ?>
		<div class="pre-itens-header">
			<div class="found-itens-quantity">
				<?php global $wp_query;
					echo sprintf(__("Showing <strong>%d</strong> of <strong>%d</strong> results", 'jeo'), $wp_query->post_count, $wp_query->found_posts);
				?>
			</div>

			<div class="sorting-method">
				<?php _e('Sort by:', 'jeo') ?>
				
				<div class="options">
					<select name="order" id="order">
						<option value="DESC" <?= isset($_GET['order']) && $_GET['order'] == 'DESC' ? 'selected' : '' ?>><?= __('Latest', 'jeo'); ?></option>
						<option value="ASC" <?= isset($_GET['order']) && $_GET['order'] == 'ASC' ? 'selected' : '' ?>><?= __('Oldest', 'jeo'); ?></option>
					</select>
				</div>

				<!-- <input type="hidden" id="sorting" name="order" value="DESC"> -->
			</div>
		</div>

		<div class="filters">
			<h5 class="filters--title"> <?php _e('Filter the list', 'jeo') ?> <span class="caret"><i class="fas fa-caret-down"></i></span> </h5>
			<div class="filters--itens">
				<?php 
				$categories = get_terms(
					array( 'taxonomy' =>  'country', 'hide_empty' => false)
				);

				//var_dump($categories);

				if(taxonomy_exists('country')):
				?>

					<div class="filters--item">
						<select multiple name="searchbycountry[]" id="countries">
							<option value=""> <?php _e('Country', 'jeo') ?> </option>
							<?php
							foreach ($categories as $term) : ?>
								<option <?= (!empty($_GET['searchbycountry']) && in_array($term->slug, $_GET['searchbycountry']))? 'selected ' : ''  ?> value="<?= $term->slug ?>" <?= isset($_GET['searchbycountry']) && $_GET['searchbycountry'] == $term->slug ? 'selected' : '' ?>> <?= $term->name ?> </option>

							<?php endforeach; ?>
						</select>
					</div>
				<?php endif ?>	
				
				
				<?php 
					$categories = get_categories();
					if($categories ):
				?>
					<div class="filters--item">
						<!-- <input type="text" placeholder="Testing" class="option-filter"> -->
						<select multiple name="topic[]" id="topics">
							<option value=""> <?php _e('Themes', 'jeo') ?> </option>
							<?php
							foreach ($categories as $term) : ?>
								<option <?= (!empty($_GET['topic']) && in_array($term->slug, $_GET['topic']))? 'selected ' : ''  ?> value="<?= $term->slug ?>" <?= isset($_GET['topic']) && $_GET['topic'] == $term->slug ? 'selected' : '' ?>> <?= $term->name ?> </option>

							<?php endforeach; ?>
						</select>
					</div>
				<?php endif ?>

				<div class="filters--item">
					<input type="text" readonly value="<?= isset($_GET['daterange']) || !empty($_GET['daterange']) ? $_GET['daterange'] : 'Date' ?>" replace-empty="<?= !isset($_GET['daterange']) || empty($_GET['daterange']) ? 'true' : 'false' ?>" autocomplete="off" placeholder="<?= __("Date", "jeo") ?>" name="daterange">
				</div>
			</div>
		</div>
	<?php endif; ?>


</form>