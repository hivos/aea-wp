<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Newspack
 */

get_header();
?>
	<section id="primary" class="content-area custom-archive">
		<div id="faceted-posts-search"></div>
	</section><!-- #primary -->
<?php
get_footer();
