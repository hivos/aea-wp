<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Newspack
 */

get_header();
?>

	<section id="primary" class="content-area row no-gutters">
			<main id="main" class="site-main">

				<h1 class="page-title article-section-title">
					<?php esc_html_e( 'Search', 'jeo' ); ?>
				</h1>
				<?php get_search_form(array(
					'aria_label' => 'search-page-form'
				)); ?>

			<?php if ( have_posts() ) : ?>

				<?php
				// Start the Loop.
				while ( have_posts() ) :
					the_post();

					template_part("content/card", ['replace_taxonomy_with_post_type' => true]);

					// End the loop.
				endwhile;

				// Previous/next page navigation.
				echo (get_theme_mod('pagination_style', 'rectangle') == 'circle'? '<div class="circle">' : '<div class="rectangle">');
                
                if ( isset( $_GET[ 'daterange' ]  ) ) {
                    $_GET[ 'daterange'] = str_replace( '/', 'x', $_GET[ 'daterange'] );
                    parse_str( $_SERVER[ 'REQUEST_URI'], $url_query );
                    $url_query['daterange'] = str_replace( '/', 'x', $_GET[ 'daterange'] );
    
                    $_SERVER[ 'REQUEST_URI' ] = http_build_query( $url_query );    
                }
                newspack_the_posts_navigation();
				echo '</div>';

				// If no content, include the "No posts found" template.
			else :
				get_template_part( 'template-parts/content/content', 'none' );

			endif;
			?>
			</main><!-- #main -->
			<aside class="category-page-sidebar">
    			<div class="content">
					<?php dynamic_sidebar('search_page_sidebar') ?>
				</div>
			</aside>
	</section><!-- #primary -->

<?php
get_footer();
