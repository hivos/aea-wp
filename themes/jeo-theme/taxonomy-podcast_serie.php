<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Newspack
 */

get_header();

$term = get_queried_object();

?>
    <section id="primary" class="content-area custom-archive">

        <?php do_action( 'before_archive_posts' ); ?>

        <main id="main" class="site-main">
            <header class="page-header">
                <span>
                    <h2 class="page-title article-section-title category-header">
                        <?= __("Radio Podcast", "jeo") ?>
                    </h2>
                    <h1>
                        <?php single_term_title(); ?>
                    </h1>
                    <div class="type-description">
                        <?= get_the_archive_description() ?>
                    </div>
                </span>
            </header><!-- .page-header -->

            <?php
            $args = [
                'post_type'   => 'audio',
                'numberposts' => 99,
                'order'       => 'DESC',
                'orderby'     => 'date',
                'meta_query'  => [
                    [
                        'key'   => 'featured_episode',
                        'value' => '1'
                    ]
                ],
                'tax_query' => [
                    [
                        'taxonomy' => 'podcast_serie',
                        'terms'    => $term->term_id
                    ]
                ]
            ];

            $featured_episodes = get_posts( $args );

            if ( $featured_episodes ) : ?>

                <section class="featured-episodes">
                    <h4><?= __("Featured episodes", "jeo") ?></h4>
                    <?php
                        foreach( $featured_episodes as $post ): ?>
                            <div class="featured-episode-content">
                                <?= get_the_content(null, false, $post->ID); ?>
                            </div>
                        <?php endforeach;
                    ?>
                </section>

            <?php endif; ?>

            <?php 
                $filters = [
                    [
                        "type"           => "taxonomy",
                        "placeholder"    => __('Subject', 'jeo'),
                        "filterSettings" => [
                            "taxonomy"          => 'post_tag',
                            "multipleSelection" => true,
                        ]
                    ],
                    [   
                        "type"           => "taxonomy",
                        "placeholder"    => __('Author', 'jeo'),
                        "filterSettings" => [
                            "taxonomy"          => 'aauthor',
                            "multipleSelection" => true,
                        ] 
                    ],
                    [
                        "type"        => "date-range-picker",
                        "placeholder" => __('Date', 'jeo'),
                    ],
                ];

                $card_model = "PostCard";
                $baseURL = get_rest_url(null, 'wp/v2/audio?podcast_serie=' . $term->term_id);

                $data_building = [
                    'filters'            => $filters,
                    'cardModel'          => $card_model,
                    'baseURL'            => $baseURL,
                    'addicionalCardMeta' => [],
                ];
            ?>
            
            <h4><?= __("All episodes", "jeo") ?></h4>

            <div id="faceted-posts-search" data-building="<?= htmlentities(json_encode($data_building)) ?>"></div>
            
        </main><!-- #main -->
        <aside class="category-page-sidebar">
            <div class="content">
                <?php dynamic_sidebar('podcast_sidebar') ?>
            </div>
        </aside>
    </section><!-- #primary -->
<?php
get_footer();
