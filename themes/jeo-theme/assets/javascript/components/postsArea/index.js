import { Fragment, useState, useEffect } from '@wordpress/element';
import { __ } from '@wordpress/i18n';
import Filters from './helpers/Filters';
import PostsList from './helpers/PostsList';

// Cards
import PostCard from './helpers/cards/PostCard';
import ResourceCard from './helpers/cards/ResourceCard';
import GenericSelector from './helpers/filters/GenericSelector';
import VideoCard from './helpers/cards/VideoCard';

import './scss/posts-area.scss';
import './scss/specific-project.scss';

window.POSTS_PER_PAGE = 6;

const baseURL = 'http://localhost/wp-json/wp/v2/posts';


const sorterFilter = {
	type: "simple-selector",
    placeholder: __('Sort by:', 'jeo'),
    filterSettings: {
        defaultOption: {
            label: __('Latests', 'jeo'),
            value: 0,
        },
        options: [
            {
                label: __('Latests', 'jeo'),
                modifyQuery: {
                    order: 'desc',
                    orderby: 'date'
                }
            },

            {
                label: __('Oldests', 'jeo'),
                modifyQuery: {
                    order: 'asc',
                    orderby: 'date'
                }
            },

            {
                label: __('Title', 'jeo'),
                modifyQuery: {
                    order: 'asc',
                    orderby: 'title'
                }
            }
        ]
    }

};

const testingFilters = [ 
    {   
        type: "search-field",
        placeholder: 'Search', 
    },
    {   
        type: "taxonomy",
        placeholder: 'Category', 
        filterSettings: { 
            taxonomy: 'category', 
            multipleSelection: true, 
        } 
    },
    {   
        type: "taxonomy",
        placeholder: 'Country', 
        filterSettings: { 
            taxonomy: 'country', 
            multipleSelection: true, 
        } 
    },

    {   
        type: "taxonomy",
        placeholder: 'Topic', 
        filterSettings: { 
            taxonomy: 'post_tag', 
            multipleSelection: true, 
        } 
    },

    {   
        type: "date-range-picker",
        placeholder: 'Date', 
    },
];

const cardModels = {
    'PostCard': PostCard,
    'ResourceCard': ResourceCard,
    'VideoCard': VideoCard
}

function FacetedPostsSearch( props ) {
    const [ totalAvaliablePosts, setTotalAvaliablePosts ] = useState(0);
    const [ totalLoadedPosts, setTotalLoadedPosts ] = useState(0); // get total posts from props
    const [ queryParams, setQueryParams ] = useState({ 'per_page': POSTS_PER_PAGE }); // get base info from props
    const [ filters, setFilters ] = useState( props.filters? props.filters : testingFilters );
    const [ hideFilterArea, setHideFilterArea ] = useState( props.hideFilterArea? props.hideFilterArea : false );
    const [ primaryFilters, setPrimaryFilters ] = useState( props.primaryFilters? props.primaryFilters : [] );


    // const [ count, setCount ] = useState(0);

    return (
        <div className="faceted-posts-search" data-filters-count={ filters.length }>
            {/* Those filters are supposed to be less generic (more specific)*/}
            { primaryFilters.length >= 1 && <div className="faceted-posts-search__generic-sort">
                <Filters filters={ primaryFilters } queryParams={ queryParams } setQueryParams={ ( queryParams ) => setQueryParams(queryParams) }/>
            </div> }
            {/* Informative and sorting  */}
            {!hideFilterArea &&
            <div className="faceted-posts-search__info-sort">
                {/* Bad practice, replace by a proper html parser */}
                <div className="quantity-info" dangerouslySetInnerHTML={ { __html: __('Showing', 'jeo') + ` <strong>${ totalLoadedPosts }</strong> ` + __('of', 'jeo') + ` <strong>${ totalAvaliablePosts }</strong> ` + __('results', 'jeo') } }></div>
                <div className="sorting-area"> 
                    <GenericSelector filter={ sorterFilter } queryParams={ queryParams } setQueryParams={ setQueryParams }/>
                </div>
            </div>}
            {/* Filtering area */}
            {!hideFilterArea && <Filters filters={ filters } queryParams={ queryParams } setQueryParams={ ( queryParams ) => setQueryParams(queryParams) }/>}
            {/* {Posts List} */}
            <PostsList CardModel={ cardModels[props.cardModel] } useInfiniteLoader={ props.useInfiniteLoader } addicionalCardMeta={ props.addicionalCardMeta } queryParams={ queryParams } baseURL={ props.baseURL? props.baseURL : baseURL } setQueryParams={ ( queryParams ) => setQueryParams(queryParams) } setTotalLoadedPosts={ setTotalLoadedPosts } setTotalAvaliablePosts={ setTotalAvaliablePosts }/>
        </div>
    );
}

const facedElement = document.getElementById( 'faceted-posts-search' );

wp.element.render(
    wp.element.createElement( FacetedPostsSearch, JSON.parse(facedElement.getAttribute('data-building')) ),
    facedElement
);