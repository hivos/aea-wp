import { Fragment, useState, useEffect } from '@wordpress/element';
import Select from 'react-select';
import './scss/generic-selector.scss';
/*
Example of a sorting filter
{
	type: "simple-selector",
    placeholder: 'Sort by',   
    filterSettings: {
        defaultOption: 'index of option (?)'
        options: [
            {
                label: 'Latests',
                modifyQuery: {
                    order: desc,
                    orderby: date
                }
            },

            {
                label: 'Oldests',
                modifyQuery: {
                    order: asc,
                    orderby: date
                }
            },

            {
                label: 'Title',
                modifyQuery: {
                    order: asc,
                    orderby: title
                }
            }
        ]
    }

}
*/


// TODO: Add multiple selection support


export default function GenericSelector({ filter, queryParams, setQueryParams }) {
    const [selectableOptions, setSelectableOptions] = useState([]);

    // First mount
    useEffect(() => {
        const filterSettings = filter.filterSettings;
        const availableOptions = filterSettings.options;

        const selectableOptionsCandidates = availableOptions.map( ( { label }, index ) => {
            return { label, value: index }
        });

        setSelectableOptions(selectableOptionsCandidates);

    }, []);

    const handleChange = ({ value }) => {
        let newQueryParams = { ...queryParams };
        const filterSettings = filter.filterSettings;
        const availableOptions = filterSettings.options;
        const optionToQuery = availableOptions[value].modifyQuery;

        newQueryParams = { ...newQueryParams, ...optionToQuery };
        setQueryParams(newQueryParams);
    }

    return (
        <div className="faceted-posts-search__generic-selector-field">
            <span className="filter-placeholder">{ filter.placeholder }</span>
            <Select
                defaultValue={filter.filterSettings.defaultOption}
                // styles={ customStyles }
                // isMulti={filter.filterSettings.multipleSelection}
                name="simple-selector"
                onChange={handleChange}
                options={selectableOptions}
                theme={selectTheme}
                className="generic-selector"
                classNamePrefix="filter"
                placeholder={filter.placeholder}
                isSearchable={false}
            />
        </div>
    )
}


// Helper functions
const selectTheme = theme => {
    // console.log(theme);
    return {
        ...theme,
        // border: 'none',
        // borderRadius: 0,
        colors: {
            ...theme.colors,
            primary25: 'var(--primary-25)',
            primary: 'var(--primary-color)',
        },
    }
};


const termToOption = ({ id, name }) => {
    return { value: id, label: name };
}