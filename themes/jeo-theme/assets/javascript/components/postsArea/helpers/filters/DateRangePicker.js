import { __ } from '@wordpress/i18n';
import { Fragment, useState, useEffect, useCallback, useRef } from '@wordpress/element';
import 'bootstrap-daterangepicker/daterangepicker.css';
import DateRangePicker from 'react-bootstrap-daterangepicker';

import './scss/date-range-picker.scss';

const locale = {
    "format": __("MM/DD/YYYY", "jeo"),
    "separator": __(" - ", "jeo"),
    "applyLabel": __("Apply", "jeo"),
    "cancelLabel": __("Clear", "jeo"),
    "fromLabel": __("From", "jeo"),
    "toLabel": __("To", "jeo"),
    "customRangeLabel": __("Custom", "jeo"),
    "daysOfWeek": [
        __("Su", "jeo"),
        __("Mo", "jeo"),
        __("Tu", "jeo"),
        __("We", "jeo"),
        __("Th", "jeo"),
        __("Fr", "jeo"),
        __("Sa", "jeo")
    ],
    "monthNames": [
        __("January", "jeo"),
        __("February", "jeo"),
        __("March", "jeo"),
        __("April", "jeo"),
        __("May", "jeo"),
        __("June", "jeo"),
        __("July", "jeo"),
        __("August", "jeo"),
        __("September", "jeo"),
        __("October", "jeo"),
        __("November", "jeo"),
        __("December", "jeo")
    ],
    "firstDay": 1
};

const initialSettings = { autoUpdateInput: false, locale };

export default function ADateRangePicker({ filter, queryParams, setQueryParams }) {
    const [inputValue, setInputValue ] = useState("");
    const dataPickerReference = useRef();

    const handleChange = (ev, picker ) => {
        console.log(ev);
        const dateOptions = [ undefined, { year:"2-digit", month:"2-digit", day:"2-digit" } ];
        const newQueryParams = { ...queryParams };

        if(ev.type === "apply") {
            setInputValue(
                picker.startDate.toDate().toLocaleDateString( ...dateOptions ) +
                ' - ' +
                picker.endDate.toDate().toLocaleDateString( ...dateOptions ),
            );

            setQueryParams({ ...newQueryParams, after: picker.startDate.toISOString(), before: picker.endDate.toISOString() } );
        } else {
            if(newQueryParams.after) {
                delete newQueryParams.after;
            }

            if(newQueryParams.before) {
                delete newQueryParams.before;
            }

            dataPickerReference.current.setStartDate(moment());
            dataPickerReference.current.setEndDate(moment());
            
            setInputValue("");
            setQueryParams(newQueryParams);
        }
        // console.log(picker);    
    }


    return (
        <div className="faceted-posts-search__date-range-picker-field">
            <DateRangePicker initialSettings={ initialSettings } onApply={ handleChange } onCancel={ handleChange } ref={ dataPickerReference }>
                <input
                    placeholder={ filter.placeholder }
                    readOnly="true"
                    value={ inputValue }
                    type="text"
                ></input>
            </DateRangePicker>
        </div>
    )
}