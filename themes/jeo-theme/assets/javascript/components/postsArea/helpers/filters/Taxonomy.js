import { Fragment, useState, useEffect } from '@wordpress/element';
import Select from 'react-select';

import './scss/taxonomy-field.scss';

const PAGE_LIMIT = 10; // if per_page is 10, 100 terms will be fetched 

/*
{   
    type: "taxonomy",
    placeholder: 'Category', 
    filterSettings: { 
        taxonomy: 'category', 
        multipleSelection: true, 
    } 
}
*/

export default function TaxonomyFilter({ filter, queryParams, setQueryParams }) {
    const [taxonomyTermsOptions, setTaxonomyTermsOptions] = useState([]);
    const [ isLoading, setIsLoading ] = useState(false);
    // First mount
    useEffect(() => {
        const taxonomy = filter.filterSettings.taxonomy;
        const targetURL = new URL(document.location.origin + `/wp-json/wp/v2/${getUrlRelativeName(filter.filterSettings.taxonomy)}`);

        // Add language param to support WPML
        if(languageParams && languageParams.currentLang) {
            targetURL.searchParams.set('lang', languageParams.currentLang);
        }

        // remove empty terms of the filter
        targetURL.searchParams.set('hide_empty', 1);

        // Fetch terms based on the filter taxonomy
        setIsLoading(true);
        fetch(targetURL)
            .then(async (response) => {
                const jsonResponse = await response.json();
                return { data: jsonResponse, totalPages: response.headers.get("x-wp-totalpages") };
            })
            .then(({ data, totalPages }) => {
                // Save first page results
                let cumulativeTerms = data;

                // If we only have one page we can return right here
                if (totalPages == 1) {
                    setTaxonomyTermsOptions(cumulativeTerms.map(termToOption));
                    // Add ready (sucess) state (animation flag)
                    setIsLoading(false);
                    return;
                }


                // Keep requesting to get to the last page
                for (let i = 2; i <= totalPages; i++) {
                    // Break to avoid respect page limiting
                    if (i >= PAGE_LIMIT) {
                        break;
                    }
                    let page_num = '?page=' + i;
                    if(languageParams && languageParams.currentLang) {
                        page_num = '&page=' + i;
                    }
                    fetch(targetURL + page_num )
                        .then(response => { return response.json() })
                        .then(moreresults => {
                            cumulativeTerms = [...cumulativeTerms, ...moreresults];

                            if (i == totalPages) {
                                // Add ready state (animation flag)
                                setTaxonomyTermsOptions(cumulativeTerms.map(termToOption));
                                setIsLoading(false);
                                // console.log(cumulativeTerms.map(termToOption));
                            }
                        });

                }
            })
            .catch(err => {
                // Remove: Too noisy 
                // alert("Error while loading posts check console");
                // console.log(err);
            })
            .then(() => {
                // Add ready state (animation flag)
                // Set done status anyway error or success ()
                setIsLoading(false);
            });

    }, []);

    const handleChange = (values) => {
        const newQueryParams = { ...queryParams };
        const taxonomy = filter.filterSettings.taxonomy;
        let urlRelativeName = getUrlRelativeName(taxonomy);

        // If handle returns undefined values then the select is empty
        if (!values) {
            newQueryParams[urlRelativeName] = '';
            setQueryParams(newQueryParams);
            return;
        }

        newQueryParams[urlRelativeName] = values.reduce(joinValuesReducer, "")

        //console.log(newQueryParams);
        setQueryParams(newQueryParams);
    }

    return (
        <div className="faceted-posts-search__taxonomy-field">
            <Select
                // defaultValue={[options[1]]}
                // styles={ customStyles }
                isMulti={filter.filterSettings.multipleSelection}
                name="taxonomy-selector"
                onChange={handleChange}
                options={taxonomyTermsOptions}
                theme={selectTheme}
                className="basic-multi-select"
                isLoading={ isLoading }
                isDisabled={ isLoading }
                classNamePrefix="select"
                placeholder={filter.placeholder}
            />
        </div>
    )
}


// Helper functions
const joinValuesReducer = function (accumulator, currentValue, index, array) {
    return (index == array.length - 1) ? accumulator + currentValue.value : accumulator + currentValue.value + ',';
}

const relativeUrlNames = {
    'category': 'categories',
    'post_tag': 'tags',
};

const getUrlRelativeName = (taxonomy) => {
    if (relativeUrlNames.hasOwnProperty(taxonomy)) {
        return relativeUrlNames[taxonomy];
    }

    return taxonomy;
}

const selectTheme = theme => ({
    ...theme,
    // borderRadius: 0,
    colors: {
        ...theme.colors,
        primary25: 'var(--primary-25)',
        primary: 'var(--primary-color)',
    },
});

const termToOption = ({ id, name }) => {
    return { value: id, label: name };
}