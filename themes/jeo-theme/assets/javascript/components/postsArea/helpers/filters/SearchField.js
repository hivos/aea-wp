import Select from 'react-select';
import { Fragment, useState, useEffect, useCallback } from '@wordpress/element';
import debounce from 'lodash.debounce';
import './scss/search-field.scss';

export default function SearchField({ filter, queryParams, setQueryParams }) {
    const [inputValue, setInputValue ] = useState("");

    const debouncedSendRequest = useCallback(
        // Debounce should notify PostsList about in progress typo (?)
		debounce(nextValue => {
            const newQueryParams = { ...queryParams };
            newQueryParams['search'] = nextValue;
            setQueryParams(newQueryParams);
        }, 500),
		[queryParams],
	);

    const handleChange = (event) => {
        // console.log(newQueryParams);
        const { value: nextValue } = event.target;
        setInputValue(nextValue);

		debouncedSendRequest(nextValue);
    }


    return (
        // This is quite a longe class name isn't it? But it follows the pattern :/
        <div className="faceted-posts-search__search-field">
            <div>
                <input type="search" placeholder={filter.placeholder} onChange={ handleChange } />
                <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z" class=""></path></svg>
            </div>
        </div>
    )
}