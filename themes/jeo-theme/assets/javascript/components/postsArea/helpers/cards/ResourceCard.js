import { Fragment, useState, useEffect } from '@wordpress/element';
import { __ } from '@wordpress/i18n';

import './scss/item-card.scss';
import './scss/resource-card.scss';

// This filters which taxonomies will be displayed
const taxonomiesThatShouldBeListed = [
    'category',
    // 'post_tag',
    // 'resource_type'
];

export default function ResourceCard({ skeleton, post, addicionalCardMeta }) {
    const [ listedTerms, setListedTerms] = useState([]);
    const [ termBasedStyle, setTermBasedStyle] = useState([]);
    const [ formats, setFormats] = useState([]);
    const [ authors, setAuthors] = useState([]);

    useEffect(() => {
        // If is a skeleton request we dont have data to process
        if(skeleton) return;

        const usefullEmbedData = post._embedded;

        if(usefullEmbedData && usefullEmbedData.hasOwnProperty("wp:term")) {
            const groupedTerms = usefullEmbedData["wp:term"];
            let ungroupedFilteredTerms =  [];

            groupedTerms.forEach( taxonomyGroup => {
                ungroupedFilteredTerms = [ ...ungroupedFilteredTerms,
                    ...taxonomyGroup.filter( term => {
                            return taxonomiesThatShouldBeListed.includes(term.taxonomy);
                    } ) ]
            })

            setListedTerms(ungroupedFilteredTerms);

            let ungroupedTypeTerms = [];

            if(addicionalCardMeta) {
                groupedTerms.forEach( taxonomyGroup => {
                    ungroupedTypeTerms = [ ...ungroupedTypeTerms,
                        ...taxonomyGroup.filter( term => {
                                return term.taxonomy == 'resource_type';
                        } ) ]
                })

                if(ungroupedTypeTerms.length) {
                    setTermBasedStyle(addicionalCardMeta.terms_meta[Object.keys(addicionalCardMeta.terms_meta).find(termId => termId  == ungroupedTypeTerms[0].id )]);
                }
            }

            let formats = [];
            let authors = [];

            groupedTerms.forEach( taxonomyGroup => {
                formats = [ ...formats,
                    ...taxonomyGroup.filter( term => {
                            return term.taxonomy == 'format';
                    } ) ]

                authors = [ ...authors,
                    ...taxonomyGroup.filter( term => {
                            return term.taxonomy == 'aauthor';
                    } ) ]
            })

            setFormats(formats);
            setAuthors(authors);


        }

    }, [ skeleton ]);


    // Ghost component
    if(skeleton) {
        return (
            <>
                <div className="item-card has-image skeleton">
                    <div className="item-card--thumbnail"></div>
                    <div className="item-card--content">
                        <div className="terms"></div>
                        <div className="title"></div>
                        <div className="additional-meta"></div>
                        <p className="excerpt"></p>
                    </div>
                </div>
            </>
        );
    } else {
        return (
            <>
                <div className={"item-card has-image"}>
                    <div className="item-card--thumbnail">
                        {/* <img src={ properImageURL } alt=""/> */}
                        <div style={ { backgroundColor: termBasedStyle? termBasedStyle.color : 'var(--primary)' }}>
                            <i className={ termBasedStyle? termBasedStyle.icon : '' }></i>
                        </div>
                    </div>

                    <div className="item-card--content">
                        {/* Main terms */}
                        {   listedTerms.length >= 1 &&
                            <div className="terms">
                                { listedTerms.map(term => {
                                    return <span>{ term.name }</span>
                                }) }
                            </div>
                        }

                        {/* Title */}
                        <a href={ post.link } className="title" dangerouslySetInnerHTML={ { __html: post.title.rendered  }}></a>

                        {/* additional meta */}
                        <div className="additional-meta">
                            { authors.length > 0 && <div className="meta-item">
                                    { __("by: ", "jeo") }

                                    { authors.map(term => {
                                        return <a href={ term.link }>{ term.name }</a>
                                    }) }
                            </div> }

                            <div className="meta-item">

                                { formats.length > 0 && <div className="format-terms">
                                    { __("format: ", "jeo") }

                                    { formats.map(term => {
                                        return <a href={ term.link }>{ term.name }</a>
                                    }) }
                                </div> }
                            </div>

                        </div>

                        {/* Bad practice, replace with a proper html parser */}
                        <p className="excerpt" dangerouslySetInnerHTML={ { __html: post.excerpt.rendered }}></p>
                    </div>
                </div>
            </>
        )

    }


}

