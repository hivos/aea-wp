import { Fragment, useState, useEffect } from '@wordpress/element';
import { __ } from '@wordpress/i18n';
import TaxonomyFilter from './filters/Taxonomy';
import SearchField from './filters/SearchField';
import ADateRangePicker from './filters/DateRangePicker';
import QuickSelection from './filters/QuickSelection';
// import GenericSelector from './filters/GenericSelector';

import './filters/scss/filters.scss';

const availableFilters = {
    'taxonomy': TaxonomyFilter,
    'search-field': SearchField,
    'date-range-picker': ADateRangePicker,
    'quick-selection': QuickSelection,
}



export default function Filters({ filters, queryParams, setQueryParams }) {
    const [ showFilters, setShowFilters ] = useState(false);

    const updateQueryParams = (queryParams) => {
        // Add new params here if you want to modify the filters behavior before the are executed
        setQueryParams({...queryParams});
    }

    // if(filters.length <= 0) {
    //     return null;
    // }

    return (
        <div className="faceted-posts-search__filters">
            {filters.length >= 1 && <div className="faceted-posts-search__filters-header">
                <button onClick={ () => setShowFilters(!showFilters) }>
                    { __("Advanced Search", "jeo") }
                    { showFilters? 
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="caret-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-caret-up fa-w-10 fa-3x"><path fill="currentColor" d="M288.662 352H31.338c-17.818 0-26.741-21.543-14.142-34.142l128.662-128.662c7.81-7.81 20.474-7.81 28.284 0l128.662 128.662c12.6 12.599 3.676 34.142-14.142 34.142z" class=""></path></svg>
                        :
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="caret-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-caret-down fa-w-10 fa-3x"><path fill="currentColor" d="M31.3 192h257.3c17.8 0 26.7 21.5 14.1 34.1L174.1 354.8c-7.8 7.8-20.5 7.8-28.3 0L17.2 226.1C4.6 213.5 13.5 192 31.3 192z" class=""></path></svg>
                    }
                    
                </button>
                
                {/* <div className="sorter">
                    <GenericSelector filter={ sorterFilter } queryParams={ queryParams } setQueryParams={ updateQueryParams }/>
                </div> */}
            </div>}
            <div className={ `faceted-posts-search__filters-list ${showFilters? 'show' : 'hide' }` }>
                { filters.map( filter => {
                    const ChosenFilter = availableFilters[filter.type];
                    
                    return <ChosenFilter filter={ filter } queryParams={ queryParams }  setQueryParams={ updateQueryParams } />
                } )}
            </div>
        </div>
    );
}

