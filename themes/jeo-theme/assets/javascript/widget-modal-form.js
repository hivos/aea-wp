window.addEventListener("DOMContentLoaded", function () {
    const modal_buttons = document.querySelectorAll('.modal-open');

    modal_buttons.forEach((button) => {
        button.addEventListener('click', () => {
            var modal_id = button.getAttribute( 'data-modal-id' );
            var $modal_bg = document.getElementById( modal_id + '-bg' );
            var $modal_content = document.getElementById( modal_id + '-content' );
            var $body = document.getElementsByTagName('body')[0];
            $body.classList.add( 'modal-open' );

            $modal_bg.style.display = '';
            $modal_bg.classList.add( 'open' );
        });
      });

      const modal_close_buttons = document.querySelectorAll('.modal-close-icon');

      modal_close_buttons.forEach((button) => {
          button.addEventListener('click', () => {
              var modal_id = button.getAttribute( 'data-modal-id' );
              var $modal_bg = document.getElementById( modal_id + '-bg' );
              var $modal_content = document.getElementById( modal_id + '-content' );
              var $body = document.getElementsByTagName('body')[0];
              $body.classList.remove( 'modal-open' );
    
              $modal_bg.style.display = 'none';
              $modal_bg.classList.remove( 'open' );
          });
        });
  
});
