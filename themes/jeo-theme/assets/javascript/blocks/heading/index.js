wp.domReady( () => {

	wp.blocks.registerBlockStyle( 'core/heading', [
		{
			name: 'heading-no-bold',
			label: 'Heading without bold',
		},
		{
			name: 'heading-500',
			label: 'Heading 500',
		},

	]);
} );