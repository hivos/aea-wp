import {
    RichText,
    InnerBlocks,
    __experimentalBlockVariationPicker,
    InspectorControls,
    BlockControls,
} from "@wordpress/block-editor";

import { __ } from "@wordpress/i18n";

import "./scss/server-side-render.scss";
import "./../../../fonts/fonts.scss";

import {
    ServerSideRender,
    Disabled,
    SelectControl,
    PanelBody,
    PanelRow,
    TextControl
} from "@wordpress/components";

const TaxonomyTermsEdit = (props) => {
    const {
        className,
        isSelected,
        attributes: { taxonomy, number },
        setAttributes
    } = props;

    const taxonimiesOptions = Object.keys(useful_info.taxonomy_list).map(value => {
        const { label } = useful_info.taxonomy_list[value];
        return {
            value,
            label
        }
    });

    return (
        <>
            <div className={className}>
                {
                    <InspectorControls>
                        <PanelBody
                            className="panel-edit-taxonomy-terms"
                            title={__("Content options", "jeo")}
                            initialOpen={true} >
                            <PanelRow>
                                <SelectControl
                                    label={__("Taxonomy:", "jeo")}
                                    value={ taxonomy } 
                                    onChange={(taxonomy) => {
                                        setAttributes({ ...props.attributes, taxonomy: taxonomy });
                                    }}
                                    options={ [
                                        {
                                            value: null,
                                            label: __("Select taxonomy", "jeo"),
                                            disabled: true,
                                        },
                                        ...taxonimiesOptions
                                    ] }
                                />
                            </PanelRow>

                            <PanelRow>
                                <TextControl
                                    label={__("Number of terms:", "jeo")}
                                    help="(use 0 (zero) to return all terms)"
                                    value={ number }
                                    onChange={(number) => {
                                        setAttributes({ ...props.attributes, number: number });
                                    }}
                                />
                            </PanelRow>
                        </PanelBody>
                    </InspectorControls>
                }

                <Disabled>
                    <ServerSideRender
                        block="jeo-theme/taxonomy-terms"
                        attributes={props.attributes}
                    />
                </Disabled>
            </div>
        </>
    );
};

export default TaxonomyTermsEdit;
