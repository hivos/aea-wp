import { registerBlockType, registerBlockVariation } from "@wordpress/blocks";
import {
    RichText,
    InnerBlocks,
    __experimentalBlockVariationPicker,
} from "@wordpress/block-editor";

import { ServerSideRender, Disabled } from "@wordpress/components";
import TaxonomyTermsEdit from './edit';

import {
    withSelect,
} from "@wordpress/data";

import { __ } from "@wordpress/i18n";
import "./scss/editor.scss";

registerBlockType("jeo-theme/taxonomy-terms", {
    title: __("Taxonomy Terms", "jeo"),
    icon: "list-view",
    category: "common",
    keywords: [__("taxonomy", "jeo"), __("list", "jeo"), __("card", "jeo")],
    supports: {
        reusable: false,
        html: false,
        customClassName: false,
        labelColor: true,
    },
    attributes: {
        className: {
            type: "string",
            default: ""
        },
        taxonomy: {
            type: "string",
            default: "category"
        },
        number: {
            type: "string",
            default: "0"
        }
    },

    styles: [
        {
            name: "default",
            label: __("Cards with background", "jeo"),
            isDefault: true,
        },
        {
            name: "with_description",
            label: __("Cards with description (inner title)", "jeo"),
        },
        {
            name: "with_description_external_title",
            label: __("Cards with description (external title)", "jeo"),
        },
    ],

    edit: TaxonomyTermsEdit, // end withSelect

    save: (props) => {
        return null;
    },
});
