import "./font-size";

window.addEventListener("DOMContentLoaded", function () {
    const header = document.querySelector(".header");

    // Sticky header events
    const stickyEvents = [
        { event: 'scroll', target: document}, 
        { event: 'resize', target: window }
    ];

    const scrollCallback = function () {
        const windowScrollTop = document.querySelector('html').scrollTop;

        if(screen.width >= 1024) {
            if(windowScrollTop > 30) {
                if(document.querySelector('body').classList.contains('single')) {
                    header.classList.add('sticky');
                }
            } else {
                header.classList.remove('sticky');
            }
        } else {
            header.classList.add('sticky');
        }
    };
    
    scrollCallback();
    stickyEvents.forEach(( { event, target } ) => target.addEventListener(event, scrollCallback));

    header.style.setProperty(
            "--padding-left",
            header.querySelector('.header--main .hamburger-menu').offsetLeft + "px"
    );

    // Open primary menu hamburguer
    header.querySelectorAll('.hamburger-menu').forEach(hamburguerIcon => hamburguerIcon.addEventListener('click', function() {
        header.querySelector('.header--content').classList.toggle('active');
        document.querySelector('body').classList.toggle('header-active');
        document.querySelector('.header--main .hamburger-menu i').classList.toggle('fa-times');
        document.querySelector('.header--sticky .hamburger-menu i').classList.toggle('fa-times');
    }));

    // Close primary menu on outside click
    document.addEventListener('click', function(event) {
        let isClickInsideElement = Array.from(header.querySelectorAll('.header--content, .hamburger-menu')).some(element => element.contains(event.target));
        let isClickInLink = Array.from(header.querySelectorAll('.header--content, .hamburger-menu .menu-item')).some(element => element.contains(event.target));
        
        // return if is svg element (tag used on submenu open element)
        if ( event.target.tagName == 'svg' ) {
            return;
        }
        
        if (!isClickInsideElement || isClickInLink) {
            header.querySelector('.header--content').classList.remove('active');
            document.querySelector('body').classList.remove('header-active');
            // console.log( 'fechou aqui' );

            header.querySelectorAll('.hamburger-menu').forEach(hamburguerMenu => {
                document.querySelector('.header--main .hamburger-menu i').classList.remove('fa-times');
                document.querySelector('.header--sticky .hamburger-menu i').classList.remove('fa-times');
            })
        }
    })

    // Open toggable search area (header)
    header.querySelectorAll('.search').forEach(search => {
        search.addEventListener('click', function() {
            header.querySelector('.header--search').classList.toggle('active');
            document.querySelector('.header--main .search i').classList.toggle('fa-times');
            document.querySelector('.header--sticky .search i').classList.toggle('fa-times');
        })
    })

    // Close toggable search area (header) on outside click
    document.addEventListener('click', function(event) {
        let isClickInsideElement = Array.from(header.querySelectorAll('.header--search, button.search')).some(element => element.contains(event.target));

        if (!isClickInsideElement) {
            header.querySelector('.header--search').classList.remove('active');
            header.querySelectorAll('button.search').forEach(searchButton => {
                let i = searchButton.querySelector('i')
                if (i) {
                    i.classList.remove('fa-times');
                }
            })
        }
    })

    // Menu itens hover - class toggle
    header.querySelectorAll('.main-menu > li').forEach(menuItem => {
        if(window.outerWidth >= 1025) {
            menuItem.addEventListener('mouseover', function() {
                header.querySelectorAll('.main-menu > li').forEach(item => item.classList.remove('active'));
                this.classList.add('active');
            })
        }

        if(window.outerWidth <= 1024) {
            menuItem.addEventListener('click', function() {
                if(!this.classList.contains('active')) {
                    this.classList.add('active');
                } else {
                    this.classList.remove('active');
                }
    
                header.querySelectorAll('.main-menu > li').forEach(item => {
                    if(item != this) {
                        item.classList.remove('active')
                    }
                });
            })
        }
    })


    // Language switcher
    const languages = document.querySelector('.language-switter');
    const actionButton = document.querySelector('button[action="toggle-language-switcher"]');
    const blockerLayer = document.querySelector('.blocker');

    if(languages) {
        actionButton.addEventListener('click', function(ev) {
            if(languages.querySelector('div')) {
                languages.classList.toggle('active');
                blockerLayer.classList.toggle('active');
            }
        })
    }

    if (blockerLayer) {
        blockerLayer.addEventListener('click', function(ev) {
            languages.classList.toggle('active');
            blockerLayer.classList.toggle('active');
        })
    }





    // Legacy scripts down bellow
    jQuery('button[action="toggle-options"]').click(function () {
        jQuery(this.parentNode.querySelector(".toggle-options")).toggleClass('active');
    });

    document.addEventListener('click', function(event) {
        var isClickInsideElement = document.querySelector('button[action="toggle-options"]').contains(event.target);
        if (!isClickInsideElement && !['increase-size', 'decrease-size'].includes(event.target.getAttribute('action'))) {
            if ( document.querySelector(".toggle-options") && document.querySelector(".toggle-options").classList ) {
                if(document.querySelector(".toggle-options").classList.contains('active')) {
                    document.querySelector(".toggle-options").classList.remove('active')
                }    
            }  
        }
    });

   
    jQuery('button[action="language-options"]').click(function () {
        jQuery(this.parentNode.querySelector(".toggle-language-options")).toggleClass(
            "active"
        );
    });

    const shareData = {
        title: document.title,
        text: "",
        url: document.location.href,
    };

    const btn = document.querySelector('button[action="share-navigator"]');
    const resultPara = document.querySelector("body");

    if(document.location.protocol != 'http:') {
        btn.addEventListener("click", () => {
            try {
                navigator.share(shareData);
            } catch (err) {
                resultPara.textContent = "Error: " + err;
            }
        });
    } else {
        console.log("Native share is not allowed over HTTP protocol.")
    }

});