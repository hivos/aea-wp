import { __ } from '@wordpress/i18n';

window.addEventListener("DOMContentLoaded", function () {
    if (document.querySelector('body').classList.contains('search')) {
        jQuery('.filters select#topics').select2({
            placeholder: __("Topics", "jeo"),
        });

        jQuery('.filters select#countries').select2({
            placeholder: __("Country", "jeo"),
        });

        const localeInformation = {
			"format": __("MM/DD/YYYY", "jeo"),
			"separator": __(" - ", "jeo"),
			"applyLabel": __("Apply", "jeo"),
			"cancelLabel": __("Cancel", "jeo"),
			"fromLabel": __("From", "jeo"),
			"toLabel": __("To", "jeo"),
			"customRangeLabel": __("Custom", "jeo"),
			"daysOfWeek": [
				__("Su", "jeo"),
				__("Mo", "jeo"),
				__("Tu", "jeo"),
				__("We", "jeo"),
				__("Th", "jeo"),
				__("Fr", "jeo"),
				__("Sa", "jeo")
			],
			"monthNames": [
				__("January", "jeo"),
				__("February", "jeo"),
				__("March", "jeo"),
				__("April", "jeo"),
				__("May", "jeo"),
				__("June", "jeo"),
				__("July", "jeo"),
				__("August", "jeo"),
				__("September", "jeo"),
				__("October", "jeo"),
				__("November", "jeo"),
				__("December", "jeo")
			],
			"firstDay": 1
		};

        jQuery('input[name="daterange"]').daterangepicker({
            minDate: "01/01/2010",
            maxDate: new Date(),
            autoUpdateInput: false,
            locale: localeInformation,
        });

        // Search fields
        jQuery('input[name="daterange"]').on("apply.daterangepicker", function (
            ev,
            picker
        ) {
            jQuery(this).val(
                picker.startDate.format("MM/DD/YYYY") +
                " - " +
                picker.endDate.format("MM/DD/YYYY")
            );

            jQuery(this).closest('form').submit();
        });

        jQuery('input[name="daterange"]').on("cancel.daterangepicker", function (
            ev,
            picker
        ) {
            jQuery(this).val("");
        });

        if (jQuery('input[name="daterange"]').attr("replace-empty") === "true") {
            jQuery('input[name="daterange"]').val("");
        }

        
        document.querySelector('.sorting-method .options select').addEventListener('change', function() {
            // console.log(this);
            this.closest('form').submit();
        })
    }
})