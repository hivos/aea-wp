window.addEventListener("DOMContentLoaded", function () {
    const allYoutubeBlocks = document.querySelectorAll('.webinar .wp-block-embed-youtube, .webinar .wp-block-video');
    if(allYoutubeBlocks.length) {
        const target = document.querySelector('.webinar .featured-area .wrapper');
        target.appendChild(allYoutubeBlocks[0]);
    }
})