window.addEventListener("DOMContentLoaded", function () {
    const termsAreas = document.querySelectorAll('.taxonomy-terms');

    termsAreas.forEach(termsArea => {
        const terms = termsArea.querySelectorAll('.taxonomy-terms-card');

        terms.forEach(termEl => {
            const icon = termEl.querySelector('.sideway i');

            if(screen.width <= 1024 && icon) {
                icon.classList.toggle('fa-minus-circle');
                icon.classList.toggle('fa-plus-circle')
                icon.closest('.taxonomy-terms-card').classList.toggle('hide-description');
            }

            if(icon) {
                icon.addEventListener('click', function(ev) {
                    this.classList.toggle('fa-minus-circle');
                    this.classList.toggle('fa-plus-circle')
                    this.closest('.taxonomy-terms-card').classList.toggle('hide-description');
                }) ;
            }
        })
    })
    
})