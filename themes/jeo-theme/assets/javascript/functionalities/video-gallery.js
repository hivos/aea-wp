window.addEventListener("DOMContentLoaded", function () {

    formatVideoGallery();

})

// Load more Vlog Series
jQuery(function ($) {
    $(".vlog-series-loadmore").click(function () {
        var button = $(this),
            data = {
                action: "loadmore",
                query: vlog_series_loadmore_params.posts,
                page: vlog_series_loadmore_params.current_page,
            };

        $.ajax({
            url: vlog_series_loadmore_params.ajaxurl, // AJAX handler
            data: data,
            type: "POST",
            beforeSend: function (xhr) {
                //button.text("...");
            },
            success: function (data) {
                if (data) {

                    // Insert the Vlog Series
                    button.prev().after(data);
                    vlog_series_loadmore_params.current_page++;

                    if (vlog_series_loadmore_params.current_page == vlog_series_loadmore_params.max_page)
                        button.remove();

                    formatVideoGallery();
                } else {
                    button.remove();
                }
            },
        });
    });
});

function formatVideoGallery() {
    document.querySelectorAll('.video-gallery-wrapper').forEach( videoGallery => {

        if (!videoGallery.classList.contains('formated')) {
            let videoItens = videoGallery.querySelectorAll('.embed-template-block');

            if(videoItens.length > 1) {
                videoCopyPolicyFix = videoItens[0].cloneNode(true);

                if(document.querySelector('body').classList.contains('cmplz-status-allow')){
                    // plugin cmplz postprocessing fix.
                    videoCopyPolicyFix.querySelector('figure > div').classList.remove('cmplz-blocked-content-container', 'cmplz-placeholder-1');
                    videoCopyPolicyFix.querySelector('figure .wp-block-embed__wrapper iframe').classList.remove('cmplz-video', 'cmplz-hidden');
                }

                videoGallery.insertBefore(videoCopyPolicyFix, videoItens[1]);
            } else if (videoItens.length == 1 && videoItens[0].querySelector('.video-excerpt')) {
                videoCopyPolicyFix = videoItens[0].querySelector('.video-excerpt').cloneNode(true);
                videoCopyPolicyFix.classList.add('video-excerpt-show');

                const excerptLimiter = document.createElement('div');
                excerptLimiter.classList.add('scroll-ratio');
                excerptLimiter.classList.add('scroll-ratio-excerpt');

                excerptLimiter.appendChild(videoCopyPolicyFix);

                videoGallery.insertBefore(excerptLimiter, videoItens[0].nextSibling);
            }

            videoItens = videoGallery.querySelectorAll('.embed-template-block');

            if(videoItens.length > 1) {
                const groupedItens = [...videoItens];
                groupedItens.splice(0, 1);

                const groupedItensWrapper = document.createElement('div');
                groupedItensWrapper.classList.add('sidebar-itens');

                const gridScrollLimiter = document.createElement('div');
                gridScrollLimiter.classList.add('scroll-ratio');
                
                let lastClicked = "";

                groupedItens.forEach(video => {
                    const clickableVideoArea = document.createElement('button');
                    clickableVideoArea.setAttribute('action', 'expand-main-area');
                    clickableVideoArea.appendChild(video);

                    clickableVideoArea.onclick = function(e) {
                        if(lastClicked != this) {
                            this.closest('.video-gallery-wrapper').querySelector('.embed-template-block').remove();
                            this.closest('.video-gallery-wrapper').insertBefore(this.querySelector('.embed-template-block').cloneNode(true), gridScrollLimiter);
                        }

                        lastClicked = this;
                    }

                    groupedItensWrapper.appendChild(clickableVideoArea);
                })

                gridScrollLimiter.appendChild(groupedItensWrapper);
                videoGallery.appendChild(gridScrollLimiter);
            }

            videoGallery.classList.add('formated');
        }
    })
}