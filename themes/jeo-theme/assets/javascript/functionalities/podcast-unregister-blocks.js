wp.domReady( () => {

    // Check if post type is `audio` by function `wp_localize_script` handle `podcast-unregister-blocks`
    if ( podcast_useful_info.current_post_type == 'audio' ) {

        const allowedBlocks = [
            'core/embed', 'core/html'
        ];

        // Unregister all blocks except `core/embed` and `core/html`
        wp.blocks.getBlockTypes().forEach( function( blockType ) {
            if ( allowedBlocks.indexOf( blockType.name ) === -1 ) {
                wp.blocks.unregisterBlockType( blockType.name );
            }
        } );

        const allowedVariationsBlocks = [
            'soundcloud', 'spotify', 'mixcloud', 'cloudup', 'youtube', 'vimeo', 'dailymotion'
        ];

        // Unregister the variations of the `core/embed` that are not in the allowedVariationsBlocks cont
        wp.blocks.getBlockType( 'core/embed' ).variations.forEach( function( blockVariation ) {
            if ( allowedVariationsBlocks.indexOf( blockVariation.name ) === -1 ) {
                wp.blocks.unregisterBlockVariation( 'core/embed', blockVariation.name );
            }
        } );
    }

} );