const ID = function () {
    return '_' + Math.random().toString(36).substr(2, 9);
};

function createInputArea(labelText, type, elementClass, onChange = () => {}, disabled=false) {
    const linkArea = document.createElement('p');
    const label = document.createElement('label');
    const linkInput = document.createElement('input');

    linkInput.setAttribute('id', labelText.toLowerCase());

    if(disabled) {
        linkInput.setAttribute('disabled', true);
    }
    // console.log(labelText);
    linkInput.addEventListener('input', onChange);

    label.innerHTML = labelText;
    linkInput.setAttribute('type', type);
    linkInput.classList.add(elementClass);
    linkArea.appendChild(label);
    linkArea.appendChild(linkInput);

    return linkArea;
}

function createButton(labelText, clickAction = () => {}, elementClasses = []) {
    const button = document.createElement('button');

    elementClasses.forEach(eClass => {
        button.classList.add(eClass);
    })

    button.innerHTML = labelText;
    button.addEventListener('click', clickAction);

    return button
}

function generateNewEventArea(id = false, title = false, link = false, authors = false) {
    const properId = !id? ID() : id;
    const area = document.createElement('div');
    area.setAttribute('data-id', properId);
    area.classList.add('event-edit-area');

    const titleArea = createInputArea('Title', 'text', 'widefat', () => {});
    area.appendChild(titleArea);

    const linkArea = createInputArea('Link', 'text', 'widefat', () => {});
    area.appendChild(linkArea);
    
    const authorsArea = createInputArea('Authors', 'text', 'widefat', () => {});
    area.appendChild(authorsArea);

    const items = [titleArea, linkArea, authorsArea];

    const updateInput = (element) => function() {
        const input = element.querySelector('input');
        const result = { id: properId };
        result[input.getAttribute('id')] = input.value;

        console.log(result);

        updateFlyer(result, area);
    }
    
    items.forEach( item => {
        item.querySelector('input').addEventListener('input', updateInput(item));
    } )

    if(title) {
        titleArea.querySelector('input').value = title;
    }

    if(link) {
        linkArea.querySelector('input').value = link;
    }

    if(authors) {
        authorsArea.querySelector('input').value = authors;
    }

    const removeButtonAction = function() {
        removeFlyer(properId, area);
        area.remove(); 
    };

    const removeButton = createButton("Remove item", removeButtonAction,['button', 'button-link-delete'])

    const buttonsArea = document.createElement('div');
    buttonsArea.classList.add('event-actions-buttons');
    buttonsArea.appendChild(removeButton);

    area.appendChild(buttonsArea);

    return area;
}

function saveFlyer(id, title, link, authors, eventElement) {
    // console.log("saveFlyer", id, link, image, eventElement);

    const storageInput = eventElement.closest('.events-list-widget').querySelector('.storage-input');
    let storageValue = JSON.parse(storageInput.value.length? storageInput.value : '[]' ) ;
    storageValue = [...storageValue, { id, title, link, authors }];
    storageInput.value = JSON.stringify(storageValue);
    storageInput.dispatchEvent(new Event('change', { 'bubbles': true }));
}

function updateFlyer(updatedFlyerProps, eventElement) {
    console.log("updateFlyer", updatedFlyerProps, eventElement);

    // Update single event on the input
    const storageInput = eventElement.closest('.events-list-widget').querySelector('.storage-input');
    let storageValue = JSON.parse(storageInput.value);
    console.log(storageInput.value, updatedFlyerProps);

    storageValue = storageValue.map(event => {
        if(event.id == updatedFlyerProps.id) {
            return {
                ...event,
                ...updatedFlyerProps,
            };
        }

        return event;
    });

    storageInput.value = JSON.stringify(storageValue);
    storageInput.dispatchEvent(new Event('change', { 'bubbles': true }));

}

function removeFlyer(id, eventElement) {
    // console.log("removeFlyer", id, eventElement);

    // Update single event on the input
    const storageInput = eventElement.closest('.events-list-widget').querySelector('.storage-input');
    let storageValue = JSON.parse(storageInput.value);

    storageValue = storageValue.filter(event => {
        return event.id !== id;
    });

    storageInput.value = JSON.stringify(storageValue);
    storageInput.dispatchEvent(new Event('change', { 'bubbles': true }));

}

function rebuildFromJson(storageInputValue, wrappingArea) {
    // Recreate widget using saved JSON
    const parsedInput = JSON.parse(storageInputValue.length? storageInputValue : '[]' );
    parsedInput.forEach(item => {
        wrappingArea.appendChild(generateNewEventArea(item.id, item.title, item.link, item.authors))
    })
}

window.addEventListener("DOMContentLoaded", function () {
    document.addEventListener('DOMSubtreeModified', function() {
        document.querySelectorAll('.events-list-widget').forEach(element => {
            if(element.getAttribute('data-processed')) {
                return;
            }

            element.setAttribute('data-processed', 'true');

            const storageInputValue = element.querySelector('.storage-input').value;
            const itensWrapping = element.querySelector('#events-options-area');
            // console.log(itensWrapping);
            rebuildFromJson(storageInputValue, itensWrapping);
            
        });
    })

    document.addEventListener('click', function(ev) {

        if(ev.target.classList.contains('add_new_event_button')) {
            const eventArea = ev.target.closest('.events-list-widget').querySelector('#events-options-area');
            const storageInput = ev.target.closest('.events-list-widget').querySelector('.storage-input');

            const newFlyerId = ID();
            eventArea.appendChild(generateNewEventArea(newFlyerId));
            saveFlyer(newFlyerId, false, false, false, eventArea);
        };
       
    })
})
;