import SSS from './../../vendor/sss/sss.min';
window.addEventListener("DOMContentLoaded", function () {
    document.querySelectorAll('.flyer-itens.mobile.slide-avaliable').forEach(function(slider, index) {        
        const sss = new SSS(slider, {
            slideShow : false, 
            startOn : 0, 
            transition : 0,
            speed : 0, 
            showNav : true
        } );

        //console.log(sss);


        const dotsArray = Array.from({length: slider.getAttribute('data-total-slides')}, (v, i) => {
            const dot = document.createElement('div');
            dot.classList.add('dot');
            dot.setAttribute('target', i);

            if(i == 0) {
                dot.classList.add('active');
            }

            return dot;
        });

        dotsArray.forEach((dot, index) => {
            dot.onclick = function() {
                jQuery(dotsArray).removeClass('active');
                dot.classList.toggle('active');
                sss.go_to_slide(index);
            }
        });

        const dotsWrapper = document.createElement('div');
        dotsWrapper.classList.add('dots-wrapper');
        dotsArray.forEach(dot => dotsWrapper.appendChild(dot))
        slider.parentNode.appendChild(dotsWrapper);

        slider.querySelector('.sssprev').addEventListener('click', function() {
            jQuery(dotsWrapper.querySelectorAll('.dot')).removeClass('active');
            jQuery(dotsWrapper.querySelectorAll('.dot')).eq(sss.target).toggleClass('active');
        });

        slider.querySelector('.sssnext').addEventListener('click', function() {
            jQuery(dotsWrapper.querySelectorAll('.dot')).removeClass('active');
            jQuery(dotsWrapper.querySelectorAll('.dot')).eq(sss.target).toggleClass('active');
        });

    })
});