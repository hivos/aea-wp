const ID = function () {
    return '_' + Math.random().toString(36).substr(2, 9);
};

function createInputArea(labelText, type, elementClass, onChange = () => {}, disabled=false) {
    const linkArea = document.createElement('p');
    const label = document.createElement('label');
    const linkInput = document.createElement('input');

    if(disabled) {
        linkInput.setAttribute('disabled', true);
    }
    // console.log(labelText);
    linkInput.addEventListener('input', onChange);

    label.innerHTML = labelText;
    linkInput.setAttribute('type', type);
    linkInput.classList.add(elementClass);
    linkArea.appendChild(label);
    linkArea.appendChild(linkInput);

    return linkArea;
}

function createButton(labelText, clickAction = () => {}, elementClasses = []) {
    const button = document.createElement('button');

    elementClasses.forEach(eClass => {
        button.classList.add(eClass);
    })

    button.innerHTML = labelText;
    button.addEventListener('click', clickAction);

    return button
}

//function generateNewFlyerArea(id, imageUrl, imageTarget) {
function generateNewFlyerArea(id = false, image = false, link = false) {
    const properId = !id? ID() : id;
    const area = document.createElement('div');
    area.setAttribute('data-id', properId);
    area.classList.add('flyer-edit-area');

    const imageSource = createInputArea('Image source', 'text', 'widefat', () => {} ,true);
    area.appendChild(imageSource);

    const linkAreaUpdate = function() {
        updateFlyer({id, link: linkArea.querySelector('input').value }, area);
    }

    const linkArea = createInputArea('Image target', 'text', 'widefat', linkAreaUpdate);
    area.appendChild(linkArea);
    
    if(image) {
        imageSource.querySelector('input').value = image;
    }

    if(link) {
        linkArea.querySelector('input').value = link;
    }

    const mediaButtonAction = function() {
        const file_frame = wp.media.frames.file_frame = wp.media({
            title: 'Select or upload image',
            library: {
                type: 'image'
            },
            button: {
                text: 'Select'
            },
            multiple: false
        });

        file_frame.on('select', function () {
            const attachment = file_frame.state().get('selection').first().toJSON();
            imageSource.querySelector('input').value = attachment.url;
            updateFlyer({id, image: attachment.url}, area);
        });

        file_frame.open();
    };

    const removeButtonAction = function() {
        removeFlyer(properId, area);
        area.remove(); 
    };

    const mediaButton = createButton("Select image", mediaButtonAction,['button', 'button-secondary'])
    const removeButton = createButton("Remove item", removeButtonAction,['button', 'button-link-delete'])

    const buttonsArea = document.createElement('div');
    buttonsArea.classList.add('flyer-actions-buttons');
    buttonsArea.appendChild(mediaButton);
    buttonsArea.appendChild(removeButton);

    area.appendChild(linkArea);
    area.appendChild(buttonsArea);

    return area;
}

function saveFlyer(id, link, image, flyerElement) {
    // console.log("saveFlyer", id, link, image, flyerElement);

    const storageInput = flyerElement.closest('.flyer-list-widget').querySelector('.storage-input');
    let storageValue = JSON.parse(storageInput.value.length? storageInput.value : '[]' ) ;
    storageValue = [...storageValue, { id, link, image }];
    storageInput.value = JSON.stringify(storageValue);
    storageInput.dispatchEvent(new Event('change', { 'bubbles': true }));
}

function updateFlyer(updatedFlyerProps, flyerElement) {
    // console.log("updateFlyer", updatedFlyerProps);

    // Update single flyer on the input
    const storageInput = flyerElement.closest('.flyer-list-widget').querySelector('.storage-input');
    let storageValue = JSON.parse(storageInput.value);

    storageValue = storageValue.map(flyer => {
        if(flyer.id == updatedFlyerProps.id) {
            return {
                ...flyer,
                ...updatedFlyerProps,
            };
        }

        return flyer;
    });

    storageInput.value = JSON.stringify(storageValue);
    storageInput.dispatchEvent(new Event('change', { 'bubbles': true }));

}

function removeFlyer(id, flyerElement) {
    // console.log("removeFlyer", id, flyerElement);

    // Update single flyer on the input
    const storageInput = flyerElement.closest('.flyer-list-widget').querySelector('.storage-input');
    let storageValue = JSON.parse(storageInput.value);

    storageValue = storageValue.filter(flyer => {
        return flyer.id !== id;
    });

    storageInput.value = JSON.stringify(storageValue);
    storageInput.dispatchEvent(new Event('change', { 'bubbles': true }));

}

function rebuildFromJson(storageInputValue, wrappingArea) {
    // Recreate widget using saved JSON
    const parsedInput = JSON.parse(storageInputValue.length? storageInputValue : '[]' );
    parsedInput.forEach(item => {
        wrappingArea.appendChild(generateNewFlyerArea(item.id, item.image, item.link))
    })
}

window.addEventListener("DOMContentLoaded", function () {
    document.addEventListener('DOMSubtreeModified', function() {
        document.querySelectorAll('.flyer-list-widget').forEach(element => {
            if(element.getAttribute('data-processed')) {
                return;
            }

            element.setAttribute('data-processed', 'true');

            const storageInputValue = element.querySelector('.storage-input').value;
            const itensWrapping = element.querySelector('#flyer-options-area');
            // console.log(itensWrapping);
            rebuildFromJson(storageInputValue, itensWrapping);
            
        });
    })

    document.addEventListener('click', function(ev) {

        if(ev.target.classList.contains('upload_image_button')) {
            const flyerArea = ev.target.closest('.flyer-list-widget').querySelector('#flyer-options-area');
            const storageInput = ev.target.closest('.flyer-list-widget').querySelector('.storage-input');

            const newFlyerId = ID();
            flyerArea.appendChild(generateNewFlyerArea(newFlyerId));
            saveFlyer(newFlyerId, false, false, flyerArea);
        };
       
    })
})
;