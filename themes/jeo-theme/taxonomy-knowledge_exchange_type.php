<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Newspack
 */

get_header();

$term = get_queried_object();
$icon_meta = get_term_meta($term->term_id, 'icon', true);
$color_meta = get_term_meta($term->term_id, 'color', true);
$background_meta = get_term_meta($term->term_id, 'background_image', true);
$image_url = false;

$knowledge_type_terms = get_the_terms(get_the_ID(), 'knowledge_exchange_type');
$knowledge_type = false;

if($knowledge_type_terms) {
    if(sizeof($knowledge_type_terms)) {
        $knowledge_type = get_term_for_default_lang($knowledge_type_terms[0]->term_id, 'knowledge_exchange_type');
    }
}


if($background_meta) {
    $image_url = wp_get_attachment_image_src($background_meta['ID'], 'full')[0];
}
?>
        <header class="page-header" style="--term-color: <?= $color_meta ?>; --term-image:  url('<?= $image_url ?>')">
            <div class="wrapper">
                <h3 class="taxonomy-title">
                  <?=  __("Knowledge Exchange", "jeo") ?>
                </h3>
                <div class="term-title">
                    <h1>
                        <i class="<?= $icon_meta ?>"></i>
                        <?php single_term_title(); ?>
                    </h1>
                </div>
                                    
                <?php if ( '' !== get_the_archive_description() ) : ?>
                    <div class="term-description d-none d-md-block">
                        <?php echo wp_kses_post( wpautop( get_the_archive_description() ) ); ?>
                    </div>
                <?php endif; ?>
                
            </div>
        </header><!-- .page-header -->
        
        <?php $show_sidebar = get_term_meta( get_queried_object_id(), 'knowledge_show_sidebar', true );?>
        <?php if ( ! $show_sidebar ) { 
             $class = 'no-sidebar';
        } else {
            $class = '';
        }
        ?>
		<section id="primary" class="content-area custom-archive <?php echo $class;?>">
			<?php do_action( 'before_archive_posts' ); ?>

			<main id="main" class="site-main">
                <?php if ( '' !== get_the_archive_description() ) : ?>
                    <div class="term-description d-md-none">
                        <?php echo wp_kses_post( wpautop( get_the_archive_description() ) ); ?>
                    </div>
                <?php endif; ?>
                <?php 

                    $filters = [];
                    if(isset($knowledge_type) && $knowledge_type->slug == 'learning-community') {

                        $filters = [
                            [   
                                "type"           => "taxonomy",
                                "placeholder"    => __('Thematic cycle', 'jeo'),
                                "filterSettings" => [
                                    "taxonomy"          => 'thematic_cycle',
                                    "multipleSelection" => true,
                                ] 
                            ],
                            [
                                "type"           => "taxonomy",
                                "placeholder"    => _x('Subject', 'Learning Community', 'jeo'),
                                "filterSettings" => [
                                    "taxonomy"          => 'post_tag',
                                    "multipleSelection" => true,
                                ]
                            ],
                            [   
                                "type" => "taxonomy",
                                "placeholder" => __('Author', 'jeo'), 
                                "filterSettings" => [ 
                                    "taxonomy" => 'aauthor', 
                                    "multipleSelection" => true, 
                                ] 
                            ],
                            [   
                                "type" => "date-range-picker",
                                "placeholder" => __('Date', 'jeo'), 
                            ],

                        ];
                    } else {
                        $filters = [
                            [
                                "type"           => "taxonomy",
                                "placeholder"    => __('Subject', 'jeo'),
                                "filterSettings" => [
                                    "taxonomy"          => 'post_tag',
                                    "multipleSelection" => true,
                                ]
                            ],
                            [   
                                "type" => "taxonomy",
                                "placeholder" => __('Author', 'jeo'), 
                                "filterSettings" => [ 
                                    "taxonomy" => 'aauthor', 
                                    "multipleSelection" => true, 
                                ] 
                            ],
                            [   
                                "type" => "date-range-picker",
                                "placeholder" => __('Date', 'jeo'), 
                            ],

                        ];
                    }

                    $card_model = "PostCard";
                    $baseURL = get_rest_url(null, 'wp/v2/knowledge_exchange?knowledge_exchange_type=' . $term->term_id);
                    $total_posts = $term->count; 

                    $terms = get_terms('knowledge_exchange_type', [
                        'orderby' => 'title',
                        'order' => 'desc',
                        'hide_empty' => false,
                        ]);
                        
                    $terms_meta = [];

                    foreach($terms as $term) {
                        $icon_meta = get_term_meta($term->term_id, 'icon', true);
                        $color_meta = get_term_meta($term->term_id, 'color', true);

                        $terms_meta[$term->term_id] = [
                            'color' => $color_meta,
                            'icon' => $icon_meta,
                        ];
                    }

                    $data_building = [
                        'filters' => $filters, 
                        'cardModel' => $card_model,
                        'baseURL' => $baseURL,
                        'addicionalCardMeta' => [  
                            'terms_meta' => $terms_meta,
                            'taxonomies_listed' => ['category'],
                            'format' => 'vertical',
                        ],
                    ];
                
                ?>

		        <div id="faceted-posts-search" data-building="<?= htmlentities(json_encode($data_building)) ?>"></div>
                
			</main><!-- #main -->
            <?php if ( $show_sidebar ) : ?>
			    <aside class="category-page-sidebar">
    			    <div class="content">
					    <?php dynamic_sidebar('knowledge_exchange_type_sidebar') ?>
				    </div>
			    </aside>
            <?php endif;?>
		</section><!-- #primary -->
<?php
get_footer();
;